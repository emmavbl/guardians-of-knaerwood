CXX     = g++
CFLAGS = -Wall -O2 -Wno-unused-result -g
LDFLAGS	= $(shell sdl2-config --libs) -lSDL2_image -lSDL2_ttf -lGLU -lGL -lglut -lm 


BINDIR	= bin/
SRCDIR	= src/
INCLUDEDIR	= include/
SDLDIR 	= /usr/include/SDL2/
OBJDIR	= obj/
EXEC 	= GoK
BIN    = $(BINDIR)GuardiansOfKnaerwoods.itd
OBJ    = main.o events.o game.o heap.o IA.o init.o load.o mouvement.o units.o display.o


all : GoK


GoK: $(OBJ)
	 g++ $(CFLAGS) -L$(SDLDIR) -o $@ $^ $(LDFLAGS) 
	@echo "FIN DE COMPILATION"

main.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c $(SRCDIR)main.cpp $(LDFLAGS) 
	@echo "======================================"


game.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)game.cpp 
	@echo "======================================"

events.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)events.cpp 
	@echo "======================================"

init.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)init.cpp $(LDFLAGS) 
	@echo "======================================"

mouvement.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)mouvement.cpp 
	@echo "======================================"

heap.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)heap.cpp 
	@echo "======================================"

IA.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)IA.cpp 
	@echo "======================================"

units.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)units.cpp  
	@echo "======================================"

load.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)load.cpp $(LDFLAGS) 
	@echo "======================================"

display.o: 
	g++ $(CFLAGS) -I $(INCLUDEDIR) -c  $(SRCDIR)display.cpp
	@echo "======================================"

clean: 
	rm -f *.o







