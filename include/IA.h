#pragma once
#include "game.h"
#include "mouvement.h"
#include "units.h"
#include <string>
#include <vector>

struct Game;
struct Menu;
struct Bonus;
struct Cell;
struct Unit;

typedef struct IA IA;
struct IA
{
    // Cout minimum du A_Star de Best Unit
    int minCost;

    // L'ID de l'equipe de d�part
    int randomEquipe;

    // Nombre d'unit�s de l'IA
    int nbUnits;

    // Si l'�quipe est cr��e --> TRUE
    bool equipeCreee;

    // Lorsque l'algo de l'IA peut �tre rapell� --> TRUE
    bool turn;

    // Si une unit� IA sort de sa moveRange --> TRUE
    bool outRange;

    // Si la target est un bonus
    bool targetIsBonus;
};

// Choisi un nom al�atoire parmis la meilleure promo IMAC de tout les temps
std::string nameOfIA(std::string nameBlue);

// Algo principal de l'IA
// ( ~83% Fonctionnel )
void IAturn(Game* game, std::vector<Unit*> units, Bonus* arrayBonus[], std::vector<Cell> arrayObs, IA* playerIA, std::vector<Cell>& rangeMove, std::vector<Cell>& rangeFight, Menu* menu, std::vector<Cell>& path, std::vector<Cell>& rangeFightBlue);
