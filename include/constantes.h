#pragma once

#include "SDL/SDL.h"

#define TAILLE_BLOC         32 // Taille d'un bloc en pixels
#define NB_BLOCS_LARGEUR    15
#define NB_BLOCS_HAUTEUR    10
#define NB_BLOCS            NB_BLOCS_HAUTEUR*NB_BLOCS_LARGEUR
#define LARGEUR_FENETRE     TAILLE_BLOC * NB_BLOCS_LARGEUR
#define HAUTEUR_FENETRE     TAILLE_BLOC * NB_BLOCS_HAUTEUR
#define aspectRatio 1280 / (float)736

/* Dimensions initiales et titre de la fenetre */
static const unsigned int WINDOW_WIDTH = 1280;
static const unsigned int WINDOW_HEIGHT = 736;
static const char WINDOW_TITLE[] = "ImacWars 2";

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 736.;


/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

