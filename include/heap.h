#pragma once

#include "mouvement.h"

// Tas Binaire
class BHeap {
private:
    std::vector <Cell> heap;
    int leftChild(int nodeIndex);
    int rightChild(int nodeIndex);
    int par(int child);
    void heapifyup(int index);
    void heapifydown(int index);
public:
    BHeap() {}
    void Insert(Cell node);
    void DeleteMin();
    Cell* ExtractMin();
    void showHeap();
    int Size();
    bool heapSearch(Cell c, int currentIndex);
    int heapSearchIndex(Cell c, int currentIndex);

    Cell* parentNode(int index);
    Cell* value(int index);
};