#pragma once

#include "game.h"
#include "mouvement.h"
#include "IA.h"

#include "GL/glew.h"
#include <vector>

struct Game;

struct IA;

struct Menu;

struct Bonus;


typedef struct Unit Unit;
struct Unit
{
    int id;

    int lvl;

    const char* type; 
    // Sorciere, fee, farfadet, demon, guerrier, orc


    // Coordonn�es sur la grille
    int x;
    int y;

    GLuint texture[3];
    int frames; //nb de frames pour l'animation de marche
    int line; //ligne du sprite pour l'animation de marche 

    //Texture des pv au dessus de l'unit�
    GLuint texturePv[3];
    
    int pv;
    int power;
    int rangeMax;
    const char* typeRange;

    //"B" ou "R"
    const char* playerID;

    // Varaibles pour l'IA
    Unit* target;
    int priorite; 
    int targetID;
};


// affiche les Unit�s
void displayUnit(Unit unit, int time);

// Mouvement des unit�s
void moveUnit(Unit* unit, int x, int y);

// Combat entre les unit�s
void fightUnits(Unit* attaquant, Unit* cible, Game* game, std::vector<Unit*>& units, int idCible, std::vector<Cell>& arrayObs);

// cr�er les unit�s
Unit* createUnit(GLuint texture[], int frames, int line, int x, int y, int id, const char* playerID, GLuint texturePV[], const char* type, const char* typeRange,int level);

// Initialise les PV d'une unit�
void initPv(Unit* unit);

// Monte d'un niveau un type d'unit� d'une equipe
void levelUp(std::vector<Unit*> units, const char* idTurn, int idLevelUp);

// Acheter des unit�s dans la caserne
void buyUnitInCaserne(Game* game, Menu* menu, IA* playerIA, std::vector<Cell>& arrayObs, std::vector<Unit*>& units, GLuint texturePV[],
    GLuint textureWitch[], GLuint textureFee[], GLuint textureFarfadet[], GLuint textureDemon[], GLuint textureGladiator[], GLuint textureOrc[]);
