#pragma once

#include "game.h"
#include <string>

/* Initialisation de la fenetre et du context OpenGL */
int initialisation(SDL_Window** window, SDL_GLContext* glcontext);

// Initialisation de Game / HUD / Menu / IA / Bonus
Game* initGame();
Hud* initHud();
Menu* initMenu();
IA* initIA();
void initBonus(Bonus* bonus, const char* type, int proba);


void createAllBonus(Bonus* arrayBonus[]);

// Charge les textures du HUD 
void loadTexutresHUD(Hud* hud, Game* game);

//Fonction de réinitialisation aprés la fin du jeu
void reInitGame(Game* game);
void reInitMenu(Menu* menu);
void reInitHud(Hud* hud);
void reInitIA(IA* playerIA);
