#pragma once
#include "units.h"
#include "mouvement.h"
#include "IA.h"

#include "GL/glew.h"
#include <vector>
#include <string>


struct Unit;

struct Cell;

struct IA;


typedef struct Player Player;
struct Player
{
    const char* id;
    std::string name;
    int money;
    int nbUnits;

    int arrayLevels[6]; // respectivement level des unit�s :  sorci�re , fee ,  farfadet, demon, guerrier, orc

    Player(const char* _id, std::string _name, int _money, int _nbUnits, int _arrayLevels[]) {};
};


typedef struct Hud Hud;
struct Hud
{
    GLuint cursorTexture[3];
    int cursorID;

    GLuint selector[3];

    // Panneau Description Unit�
    GLuint pannelDescBlue[3];
    GLuint pannelDescRed[3];
    GLuint type[3];
    GLuint lvl[3];
    GLuint pv[3];
    GLuint power[3];
    GLuint story[3];
    bool changeDescription; // met a jour la description de l'unit� dans le hud

    // Panneau Joueur 
    GLuint pannelPlayerBlue[3];
    GLuint pannelPlayerRed[3];
    GLuint pseudo[3];
    GLuint money[3];

    const char* side;

};

typedef struct Game Game;
struct Game
{
    Player * playerBlue;
    Player * playerRed;
    
    int nbPlayers;

    // Id du joueur � qui c'est le tour : "B" ou "R"
    const char* idTurn;

    // Diff�rents modes de jeu
    bool modeMove;
    bool modeFight;
    bool modeSelect;
    bool modeAchat;
    bool modeTuto;

    // Num�ro �tape dans le tuto (4 tutos en tout)
    int nbTuto;

    // Changement de jour
    GLuint textureChangeDay[3];
    int dayCount;
    bool changeDay;

    //Prix des unit�s, respectivement : sorci�re , fee ,  farfadet, demon, guerrier, orc
    int priceUnit[6];

    // Case survol�e par la souris
    int caseX;
    int caseY;

    // Position absolue de la souris
    int cursorX;
    int cursorY;

    // Coordonn�es de la case d'arriv�e pour un mouvement d'unit�
    int moveX;
    int moveY;

    // Diff�rents identifiants des unit�s
    int idSelect;
    int idCible;
    int idDescription;

    // Si c'est la fin du jeu --> TRUE
    bool endGame;

    // Cases pour le A_Star
    Cell* startNode;
    Cell* endNode;

    // Index dans le chemin du A_Star pour que le mouvement de l'unit� suive les cases une par une
    int indexPathMove;

    // Nombre d'actions des joueurs par tour --> 3 max
    int countActions;

    // Si on joue contre l'IA
    bool IA;

};

typedef struct Menu Menu;
struct Menu
{   
    // Type de menu 
    bool choiceNbPlayers;
    bool choicePseudo;
    bool auberge;
    bool caserne;

    // Mettre � jour les menus Auberge et Caserne
    bool reloadMenu;

    // Identifiants du choix dans un menu
    int idImgPseudo;
    int idLevelUp;
    int idAchat;

    // Position du curseur dans un menu
    int cursorX;
    int cursorY;

    // Zone de texte
    bool allowed_to_type_again;
    bool inputTextInFocus;
    GLuint inputTexturePseudo[3];

};


typedef struct Bonus Bonus;
struct Bonus
{   
    // Coordonn�es d'un Bonus
    int caseX;
    int caseY;

    GLuint Texture[3];

    // Type d'un bonus : (chest, coin, fiole, cheese)
    std::string type;

    // Probabilit� d'apparition d'un bonus
    int proba;

    Bonus(int _caseX, int _caseY, GLuint _Texture[3], std::string _type, int _proba) {};
};


/* Initialisation du joueur */
void initPlayer(Player* joueur, const char* id, const char* name);

/* cr�ation des obstacle a partir d'une matrice de 0 et de 1 */
void createObstacles(std::vector<Cell>& arrayObs);

/* Ajoute les unit�s aux obstacles */
void updateObstacles(std::vector<Unit*> units, std::vector<Cell>& arrayObs, Game* game);

/* Enl�ve les ennemis du joueur actif aux obstacles pour permettre l'attaque */
void removeEnnemyFromObstacles(std::vector<Unit*> units, std::vector<Cell>& arrayObs, Game* game);

/* Enl�ve un ennemi du joueur actif aux obstacles pour permettre l'attaque */
void removeOneUnitFromObstacles(std::vector<Unit*> units, int idUnit, std::vector<Cell>& arrayObs, Game* game);

/* Affiche les bonus sur la carte*/
void displayBonus(Bonus * arrayBonus[], std::vector<Cell> arrayObs);

/* Gen�re la positions des bonus al�atoirement sur la carte*/
void updateBonus(Bonus* arrayBonus[], std::vector<Cell> arrayObs);

/* Lorsque une unit� est sur un bonus, modifie les param�tres du joueurs ou de l'unit� */
void hitBonus(Unit* unit, Bonus* bonus, Player* player, Hud* hud);

/* Algorithme principal pour les diff�rentes phases de jeu : 
- FightMode
- Mouvement d'une unit�
- Arriv� a la destination
- Changement de tour / nb d'actions max atteint
*/
void gameModes(Game* game, IA* playerIA, std::vector<Unit*>& units, Hud* hud, std::vector<Cell>& arrayObs, Bonus* arrayBonus[],
    std::vector<Cell>& path, std::vector<Cell> rangeMove, std::vector<Cell> rangeFight, std::vector<Cell> rangeFightBlue);

/* Reinitialise toutes les variables de jeu pour permettre de rejouer indefiniment */
void reInitGame(Game* game, Hud* hud, Menu* menu, IA* playerIA, std::vector<Unit*>& units, std::string& inputPseudo, std::vector<Cell>& path,
    std::vector<Cell>& rangeMove, std::vector<Cell>& rangeFight, std::vector<Cell>& arrayObs, std::vector<Cell>& rangeFightBlue, Bonus* arrayBonus[]);
