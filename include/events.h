#pragma once

#include "game.h"
#include <vector>
#include <string>
#include "SDL/SDL.h"


/*
FONCTION DE TRAITEMENT DES EVENEMENTS
Clic souris, Mouvement Souris, touches claviers
S�lection des unit�s, activation des modes de jeu (Move, Fight, Menu...)
Traite aussi la navigation des menus ainsi que les entr�es de textes pour le choix du pseudo
*/
void manageEvents(Game* game, Hud* hud, Menu* menu, int& runGame, std::vector<Unit*>& units, std::string& inputPseudo, std::vector<Cell>& rangeMove, 
	std::vector<Cell>& rangeFight, std::vector<Cell>arrayObs, std::vector<Cell>& path, IA* playerIA, std::vector<Cell>& rangeFightBlue, Bonus* arrayBonus[]);

/*
Verifie la position d'un clic sur la map, soit sur l'auberge, soit sur la caserne
*/
bool isAuberge(SDL_Event e);
bool isCaserne(SDL_Event e);
