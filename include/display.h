#pragma once
#include "game.h"

#include "GL/glew.h"
#include <vector>
#include <string>


/*
FONCTION PRINCIPALE D'AFFICHAGE DE TEXTURE
Affiche la texture selon les coordonn�es xDst et yDst
*/
void displayTexture(GLuint Texture[], double xSrc, double ySrc, double xDst, double yDst, int w_dst, int h_dst);

/*
Affiche la carte et les unit�s
*/
void displayMapAndUnits(Game* game, std::vector<Unit*> units, int seconds, GLuint Carte[], GLuint Grille[]);

/*
Affiche l'ensemble des textures li�es au HUD
*/
void displayHUD(Game* game, Hud* hud, int& timeCountDay, int seconds, std::vector<Unit*> units);

/*
Affiche le texte du changement de jour
*/
void displayChangeDay(int day, GLuint Texture[], int timeCountDay);

/*
Affiche le texte de fin du jeu avec le gagnant de la parite
*/
void displayEndGame(Game* game, std::string textEndgame, const char* colorEndgame, GLuint endGameText[], bool& firstFrameEndgame, GLuint pressEnter[]);

/*
Affiche les diff�rents menu (Auberge et Caserne)
*/
void displayMenuAuberge(Menu* menu, Game* game, GLuint prixLevel[][3], GLuint level[][3], GLuint playerMoney[]);
void displayMenuCaserne(Menu* menu, Game* game, GLuint playerMoney[]);