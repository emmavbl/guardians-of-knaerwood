#pragma once

#include "units.h"
#include "game.h"

#include<vector>

struct Game;

struct Unit;

/* Structure cellule pour le A_star */
typedef struct Cell Cell;
struct Cell
{
    int x;
    int y;

    int F;  // F = G + H
    int G;  // G = Disstance between the current node and the start node
    int H;  // Estimated distance between the current node and the end node 

    Cell* parent;

    bool walkable;
    bool isInRange;
};

#define TAILLE_BLOC 32

void initCell(int x, int y, Cell* cell, bool walkable);

// Verifie si une cellule est dans un tableau de cellule
bool isInside(Cell cell, std::vector<Cell> array);

// Idem + renvoie l'index
int isInsideIndex(Cell cell, std::vector<Cell> array);

// Si deux cellules ont les m�mes coordonn�es --> TRUE
bool isSameCell(Cell c1, Cell c2);

// Algo du plus court chemin pour les d�placement et les cases disponibles dans les ranges des unit�s
// Retourne le nombres de cases du chemin calcul�
int A_Star(Cell start, Cell end, std::vector<Cell> const arrayObstacle, std::vector<Cell>& path, bool savePath);

// Cr�e les ranges (tableau de cellules) de mouvement et d'attaque autour de l'unit�
void createRanges(Cell startNode, std::vector<Cell>& rangeMove, std::vector<Cell>& rangeFight, std::vector<Cell> arrayObs, Unit unit, std::vector<Unit*> units, Game* game, bool onlyFight);

// Si une cellule est dans la map --> TRUE
bool isInGrid(Cell cell);

