#pragma once
#include "GL/glew.h"

/*
FONCTION QUI CHARGE LES TEXTURES
Modifie le tableau Texture :
Texture[0] = identifiant de la texture
Texture[1] = largeur de la texture
Texture[2] = hauteur de la texture
*/
GLuint loadTexture(const char* filename, GLuint Texture[]);

// FONCTION QUI CHARGE LES FONT EN TANT QUE TEXTURE
GLuint loadFont(const char* text, int size, const char* color, GLuint Texture[]);