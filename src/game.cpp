#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE 

#include "game.h"
#include "constantes.h"
#include "load.h"
#include "display.h"
#include "units.h"
#include "IA.h"
#include "init.h"

#include <iostream>

#include "string.h"
#include "GL/glew.h"
#include<string>
#include<vector>


void initPlayer(Player* joueur, const char* id, const char* name) {
	joueur->id = id;
	joueur->name = name;
	joueur->money = 1000;
    joueur->nbUnits = 0; 
    for (int i = 0; i < 6; i++)
        joueur->arrayLevels[i] = 1;
}


void createObstacles(std::vector<Cell>& arrayObs) {
    if (arrayObs.size() > 0)
        arrayObs.clear();

    const char* matriceObs[23] = { "1111111000010000000001111111111100000000",
                                    "1111111000010000000001111111111100000000",
                                    "1101111000011111111001111111111100000000",
                                    "1000111111111111111111111111111111100000",
                                    "1000111111111111111001111111111111111111",
                                    "1000111111111111000000111111111111111111",
                                    "1000111111111110000000011111111111111111",
                                    "1111111111111100001100011100001111111111",
                                    "1111111111111000111110000100011111111111",
                                    "1111111111110001111111000100011111111111",
                                    "1111111111110001111111000111111111111111",
                                    "1111111111111001111011100111111111110111",
                                    "1111111111111111111111111111111001100011",
                                    "1111111111110001111111100111111001100011",
                                    "1110111011110000111111000111110000100011",
                                    "1100010001111000011110000111110000100011",
                                    "1100010000111100000000001111110000100000",
                                    "1110111000011110000000111111111111111111",
                                    "1111111100011111111111111111111110101111",
                                    "1011011110111111000111111111111110101111",
                                    "0000001011111110001111111111111110000000",
                                    "0000000001111110011111111111111110000000",
                                    "0000000001111110011111111111111110000000 " };

    for (int i = 0; i < 23; i++)
    {
        for (int j = 0; j < 40; j++) {
            if (matriceObs[i][j] == '0') {
                Cell caseObs;
                initCell(j * TAILLE_BLOC, i * TAILLE_BLOC, &caseObs, false);
                arrayObs.push_back(caseObs);
            }
        }
    }
}


void updateObstacles(std::vector<Unit*> units, std::vector<Cell>& arrayObs, Game * game) {
    // R�initialiser la matrice obstacle
    createObstacles(arrayObs);

    // D�finir les positions actuelles des Units en tant qu'obstacles
    for (int i = 0; i < units.size(); i++)
    {
        Cell caseObs;
        initCell(units[i]->x, units[i]->y, &caseObs, false);
        arrayObs.push_back(caseObs);
    }
}

void removeEnnemyFromObstacles(std::vector<Unit*> units, std::vector<Cell>& arrayObs, Game* game) {
    // R�initialiser la matrice obstacle
    createObstacles(arrayObs);

    // D�finir les positions actuelles des Units en tant qu'obstacles
    for (int i = 0; i < units.size(); i++)
    {
        if (units[i]->playerID == game->idTurn) {
            Cell caseObs;
            initCell(units[i]->x, units[i]->y, &caseObs, false);
            arrayObs.push_back(caseObs);
        }  
    }
}

void removeOneUnitFromObstacles(std::vector<Unit*> units, int idUnit, std::vector<Cell>& arrayObs, Game* game) {
    // R�initialiser la matrice obstacle
    createObstacles(arrayObs);

    // D�finir les positions actuelles des Units en tant qu'obstacles, sauf celle de l'unit� en question
    for (int i = 0; i < units.size(); i++)
    {
        if (i != idUnit) {
            Cell caseObs;
            initCell(units[i]->x, units[i]->y, &caseObs, false);
            arrayObs.push_back(caseObs);
        }
    }
}

void displayBonus(Bonus* arrayBonus[], std::vector<Cell> arrayObs) {

    for (int i = 0; i < 4; i++) {
        if (arrayBonus[i]->caseX != -1 && arrayBonus[i]->caseY != -1)
            displayTexture(arrayBonus[i]->Texture, 0, 0, arrayBonus[i]->caseX, arrayBonus[i]->caseY, arrayBonus[i]->Texture[1], arrayBonus[i]->Texture[2]);
    }

}

void updateBonus(Bonus* arrayBonus[], std::vector<Cell> arrayObs) {
    int xRand = -1;
    int yRand = -1;
    int r = -1;
    Cell bonusCell;
    initCell(-1, -1, &bonusCell, true);

    for (int i = 0; i < 4; i++) {
        if (arrayBonus[i]->caseX == -1 && arrayBonus[i]->caseY == -1) {
            r = rand() % 100;
            if (r <= arrayBonus[i]->proba) {
                do {
                    xRand = (rand() % 40) * TAILLE_BLOC;
                    yRand = (rand() % 23) * TAILLE_BLOC;
                    bonusCell.x = xRand;
                    bonusCell.y = yRand;
                } while (isInside(bonusCell, arrayObs));
                arrayBonus[i]->caseX = xRand;
                arrayBonus[i]->caseY = yRand;
            }
        }
        
    }
}


void hitBonus(Unit* unit, Bonus* bonus, Player* player, Hud * hud){

    if (bonus->type == "chest") {
        player->money += 500;
        std::string money = std::to_string(player->money);
        loadFont(money.c_str(), 24, "N", hud->money);
    } 
    else if (bonus->type == "fiole") {
        unit->pv += 20;
        initPv(unit);
    }
    else if (bonus->type == "coin") {
        player->money += 100;
        std::string money = std::to_string(player->money);
        loadFont(money.c_str(), 24, "N", hud->money);
    }
    else { 
        unit->pv += 10;
        initPv(unit);
    }

    bonus->caseX = -1;
    bonus->caseY = -1;

}


void gameModes(Game * game, IA * playerIA, std::vector<Unit*>& units, Hud * hud, std::vector<Cell>& arrayObs, Bonus * arrayBonus[], 
    std::vector<Cell>& path, std::vector<Cell> rangeMove, std::vector<Cell> rangeFight, std::vector<Cell> rangeFightBlue) {
    
    // Fight Mode
    if (game->modeFight) {

        if (game->IA && game->idTurn == "R") {
            playerIA->turn = true;
        }

        fightUnits(units[game->idSelect], units[game->idCible], game, units, game->idCible, arrayObs);

        game->modeFight = false;
        game->countActions++;
        game->idSelect = -1;
    }
       
    // Lorsqu'on a atteint la position d'arriv�
    if (game->modeMove && game->idSelect != -1 && units[game->idSelect]->x == game->moveX && units[game->idSelect]->y == game->moveY
        || (game->IA && game->idTurn == "R" && playerIA->outRange)) {

        if (game->IA && game->idTurn == "R") {
            playerIA->turn = true;
            playerIA->outRange = false;
        }

        // Si on arrive sur un Bonus
        for (int i = 0; i < 4; i++)
        {
            if (arrayBonus[i]->caseX == game->moveX && arrayBonus[i]->caseY == game->moveY) {
                if (units[game->idSelect]->playerID == "B")
                    hitBonus(units[game->idSelect], arrayBonus[i], game->playerBlue, hud);
                else {
                    hitBonus(units[game->idSelect], arrayBonus[i], game->playerRed, hud);
                }
            }
        }

        game->modeMove = false;
        game->countActions++;
        game->idSelect = -1;
        game->moveX = -1;
        game->moveY = -1;
        path.clear();

        updateObstacles(units, arrayObs, game);
    }

    // Lorsqu'un joueur a effectu� le nombre d'action max
    if (game->idSelect == -1 && game->countActions >= 3) {
        if (game->idTurn == "B") {
            path.clear();
            game->idTurn = "R";
            std::string money = std::to_string(game->playerRed->money);
            loadFont(money.c_str(), 24, "N", hud->money);

            loadFont(game->playerRed->name.c_str(), 28, "N", hud->pseudo);
        }
        else {
            // Remettre les priorit�s des unit�s IA � 3
            if (game->IA) {
                for (int i = 0; i < units.size(); i++) {
                    if (units[i]->playerID == "B")
                        continue;
                    units[i]->priorite = 4;
                }
            }
            // Updates les bonus
            updateBonus(arrayBonus, arrayObs);
            // Vider le vecteur path
            path.clear();

            game->idTurn = "B";
            game->changeDay = true;
            std::string money = std::to_string(game->playerBlue->money);
            loadFont(money.c_str(), 24, "N", hud->money);
            loadFont(game->playerBlue->name.c_str(), 28, "N", hud->pseudo);
        }
        game->countActions = 0;
    }

    // Move Mode --> Algorithme de Mouvement
    if (game->idSelect != -1 && game->modeMove && game->indexPathMove >= 0) {

        // Arriv� dans une nouvelle case
        if (units[game->idSelect]->x == path[game->indexPathMove].x && units[game->idSelect]->y == path[game->indexPathMove].y) {
            if (isInside(path[game->indexPathMove], rangeFightBlue) && !playerIA->targetIsBonus) {
                playerIA->outRange = true; // Prio 2 --> Est dans la range Fight de l'ennemi
            }
            game->indexPathMove--;
            if (game->indexPathMove < 0 && playerIA->targetIsBonus) {
                playerIA->outRange = true;
            }
        }

        // Mouvement de l'unit� IA
        if (game->IA && game->idTurn == "R" && !playerIA->outRange && game->indexPathMove >= 0) {
            if (isInside(path[game->indexPathMove], rangeMove) || isInside(path[game->indexPathMove], rangeFight)) {
                moveUnit(units[game->idSelect], path[game->indexPathMove].x, path[game->indexPathMove].y);
            }
            else {
                playerIA->outRange = true; // Prio 4 --> Sort de sa range Move
            }
        }
        // Mouvement de l'unit� Joueur
        else if (!game->IA || (game->IA && game->idTurn == "B")) {
            moveUnit(units[game->idSelect], path[game->indexPathMove].x, path[game->indexPathMove].y);
        }
    }
}

void reInitGame(Game* game, Hud* hud, Menu* menu, IA* playerIA, std::vector<Unit*>& units, std::string& inputPseudo, std::vector<Cell>& path,
    std::vector<Cell>& rangeMove, std::vector<Cell>& rangeFight, std::vector<Cell>& arrayObs, std::vector<Cell>& rangeFightBlue, Bonus * arrayBonus[]) {
    
    reInitGame(game);
    reInitHud(hud);
    reInitMenu(menu);
    reInitIA(playerIA);

    for (int i = 0; i < 4; i++)
    {
        arrayBonus[i]->caseX = -1;
        arrayBonus[i]->caseY = -1;
    }
    updateBonus(arrayBonus, arrayObs);

    int array[6] = { 1 };
    game->playerBlue = new Player("B", "Thomas", 200, 3, array);
    game->playerRed = new Player("R", "Emma", 200, 3, array);
    initPlayer(game->playerBlue, "B", "Thomas");
    initPlayer(game->playerRed, "R", "Emma");
    
    units.clear();
    
    inputPseudo = "";
    loadFont(inputPseudo.c_str(), 26, "N", menu->inputTexturePseudo);

    path.clear();
    rangeMove.clear();
    rangeFight.clear();
    arrayObs.clear();
    rangeFightBlue.clear();

}

