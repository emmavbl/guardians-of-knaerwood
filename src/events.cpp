#include "events.h"
#include "game.h"
#include "constantes.h"
#include "units.h"
#include "load.h"
#include "display.h"
#include "mouvement.h"
#include "IA.h"
#include "init.h"

#include "SDL/SDL.h"
#include <iostream>
#include <string>
#include <vector>



void manageEvents(Game* game, Hud* hud, Menu* menu, int& runGame, std::vector<Unit*>& units, std::string& inputPseudo, std::vector<Cell>& rangeMove, 
    std::vector<Cell>& rangeFight, std::vector<Cell>arrayObs, std::vector<Cell>& path, IA * playerIA, std::vector<Cell>& rangeFightBlue, Bonus* arrayBonus[]) {
    SDL_Event e;
    bool changeCase = false;
    
    bool isInRangeMove = false;
    bool isInRangeFight = false;

    // Case Hover on range
    Cell currentCell;
    initCell(-1, -1, &currentCell, false);

    // Case Click Map
    Cell targetCase;
    initCell(-1, -1, &targetCase, false);

    
    while (SDL_PollEvent(&e))
    {
        /* L'utilisateur ferme la fenetre : */
        if (e.type == SDL_QUIT)
        {
            runGame = 0;
            break;
        }

        switch (e.type)
        {
            /* Mouvement souris */
        case SDL_MOUSEMOTION:
            game->cursorX = e.motion.x;
            game->cursorY = e.motion.y;

            game->caseX = e.motion.x / TAILLE_BLOC * TAILLE_BLOC;
            game->caseY = e.motion.y / TAILLE_BLOC * TAILLE_BLOC;

            currentCell.x = game->caseX;
            currentCell.y = game->caseY;

            


            if (e.motion.x / TAILLE_BLOC * TAILLE_BLOC == game->endNode->x / TAILLE_BLOC * TAILLE_BLOC
                && e.motion.y / TAILLE_BLOC * TAILLE_BLOC == game->endNode->y / TAILLE_BLOC * TAILLE_BLOC) {
                changeCase = false;
            }
            else {
                changeCase = true;
            }

            /*std::cout << "Current Case X " << currentCell.x << std::endl;
            std::cout << "Current Case Y " << currentCell.y << std::endl;
            std::cout << "===================" << std::endl;*/

            if (game->modeSelect && game->idSelect != -1 && units[game->idSelect]->playerID == game->idTurn 
                && (isInside(currentCell, rangeMove) || isInside(currentCell, rangeFight))) {
                
                if (changeCase) {
                    if (isInside(currentCell, rangeFight)) {
                        hud->cursorID = 0;
                    }
                    else {
                        hud->cursorID = 1;
                    }
                    path.clear();
                    game->endNode->x = e.motion.x / TAILLE_BLOC * TAILLE_BLOC;
                    game->endNode->y = e.motion.y / TAILLE_BLOC * TAILLE_BLOC;
                    A_Star(*game->startNode, *game->endNode, arrayObs, path, true);
                }
                
            }else if (isAuberge(e) || isCaserne(e)) {
                hud->cursorID = 2;
            }
            else {
                hud->cursorID = 3;
            }


            break;

            /* Clic souris */
        case SDL_MOUSEBUTTONUP:

            if (!game->changeDay) {
                targetCase.x = e.button.x / TAILLE_BLOC * TAILLE_BLOC;
                targetCase.y = e.button.y / TAILLE_BLOC * TAILLE_BLOC;

                if (isInside(targetCase, rangeMove))
                    isInRangeMove = true;
                else
                    isInRangeMove = false;

                if (isInside(targetCase, rangeFight))
                    isInRangeFight = true;      
                else
                    isInRangeFight = false;


                //Click sur l'auberge
                if (isAuberge(e)) {
                    std::cout << "Dans l'Auberge" << std::endl;
                    menu->auberge = true;
                    menu->reloadMenu = true;
                    menu->cursorX = 11 * TAILLE_BLOC;
                    menu->cursorY = 10 * TAILLE_BLOC;

                    game->idSelect = -1;
                    game->modeSelect = false;
                    game->idDescription = -1;

                }
                else if (isCaserne(e)) {
                    std::cout << "Dans la caserne" << std::endl;
                    menu->caserne = true;
                    menu->reloadMenu = true;
                    menu->cursorX = 9.5 * TAILLE_BLOC;
                    menu->cursorY = 6.5 * TAILLE_BLOC;
                    menu->idImgPseudo = 1;

                    game->idSelect = -1;
                    game->modeSelect = false;
                    game->idDescription = -1;
                }

                // Boucle sur toutes les unites
                for (int i = 0; i < units.size(); i++) {
                    // Click sur Alli�
                    if (units[i]->x == e.button.x / 32 * 32 && units[i]->y == e.button.y / 32 * 32 && units[i]->playerID == game->idTurn) {
                        game->idSelect = i;

                        if (!game->modeSelect) {
                            rangeMove.clear();
                            rangeFight.clear();
                            game->modeSelect = true;
                            game->modeMove = false;
                            game->startNode->x = e.button.x / TAILLE_BLOC * TAILLE_BLOC;
                            game->startNode->y = e.button.y / TAILLE_BLOC * TAILLE_BLOC;
                            createRanges(*game->startNode, rangeMove, rangeFight, arrayObs, *units[i], units, game, false);
                        }
                        else {
                            game->modeSelect = false;
                            game->modeMove = false;
                        }
                        break;
                    }

                    // Click sur Ennemi
                    if (units[i]->x == e.button.x / 32 * 32 && units[i]->y == e.button.y / 32 * 32 && units[i]->playerID != game->idTurn && isInRangeFight) {
                        game->modeMove = false;
                        std::cout << "Ennemy Selected" << std::endl;
                        if (!game->modeFight && game->modeSelect) {
                            std::cout << "Fight" << std::endl;
                            game->modeSelect = false;
                            game->modeFight = true;
                            game->idCible = i;
                        }
                        else {
                            game->idSelect = i;
                        }
                        break;
                    }
                    
                    if (game->modeSelect && (isInRangeMove || isInRangeFight))
                        game->modeMove = true;
                    else if (!game->modeMove) {
                        game->idSelect = -1;
                        game->modeSelect = false;
                        path.clear();
                    }
                }

                // END BOUCLE UNITE 

                // Click sur aucune unit�
                if (game->modeMove && !game->modeFight && game->modeSelect) {
                    game->modeSelect = false;
                    game->moveX = e.button.x / TAILLE_BLOC * TAILLE_BLOC;
                    game->moveY = e.button.y / TAILLE_BLOC * TAILLE_BLOC;
                    game->indexPathMove = path.size() - 1;
                }

                // DEBUG
                std::cout << "selectMode " << game->modeSelect << std::endl;
                std::cout << "moveMode " << game->modeMove << std::endl;
                std::cout << "fightMode " << game->modeFight << std::endl;
                std::cout << "=====================" << std::endl;
            }
            

            

            break;


        case SDL_TEXTINPUT:
            if (menu->choicePseudo && menu->allowed_to_type_again) {
                if (inputPseudo.length() < 9 && menu->inputTextInFocus) {
                    menu->allowed_to_type_again = false;
                    inputPseudo += e.text.text;
                    loadFont(inputPseudo.c_str(), 36, "N", menu->inputTexturePseudo);
                }
            }
            break;


            /* Touche clavier */
        case SDL_KEYDOWN:
            


            if (menu->choicePseudo && menu->allowed_to_type_again) {
                if (menu->inputTextInFocus && e.key.keysym.sym == SDLK_BACKSPACE && inputPseudo.length() > 0) {
                    menu->allowed_to_type_again = false;
                    inputPseudo = inputPseudo.substr(0, inputPseudo.length() - 1);
                    loadFont(inputPseudo.c_str(), 36, "N", menu->inputTexturePseudo);
                }
            }

            switch (e.key.keysym.sym)
            {

            case SDLK_UP:
                if (menu->choiceNbPlayers) {
                    menu->cursorX = 350;
                    menu->cursorY = 350;
                }

                if (menu->choicePseudo || menu->caserne) {
                    if (menu->cursorY == 6.5 * TAILLE_BLOC) {
                        menu->cursorY += (7 * TAILLE_BLOC);
                        menu->idImgPseudo += 3;

                    }   
                    else {
                        menu->cursorY -= (7 * TAILLE_BLOC);
                        menu->idImgPseudo -= 3;
                    }
                }

                if (menu->auberge) {
                    if (menu->cursorY == 10 * TAILLE_BLOC) {
                        menu->cursorY = 14 * TAILLE_BLOC;
                        menu->idLevelUp += 4;

                    }
                    else {
                        menu->cursorY -= 2 * TAILLE_BLOC;
                        menu->idLevelUp -= 2;

                    }
                }
                break;

            case SDLK_DOWN:
                if (menu->choiceNbPlayers) {
                    menu->cursorX = 350;
                    menu->cursorY = 485;
                }

                if (menu->choicePseudo || menu->caserne) {
                    if (menu->cursorY == 6.5 * TAILLE_BLOC)
                        menu->idImgPseudo += 3;
                    else
                        menu->idImgPseudo -= 3;
                    menu->cursorY += (7*TAILLE_BLOC);
                    menu->cursorY %= 2 * (7 * TAILLE_BLOC);
                }

                if (menu->auberge) {
                    if (menu->cursorY == 14 * TAILLE_BLOC) {
                        menu->cursorY = 10 * TAILLE_BLOC;
                        menu->idLevelUp -= 4;
                    }
                    else {
                        menu->cursorY += 2 * TAILLE_BLOC;
                        menu->idLevelUp += 2;
                    }

                }


                break;

            case SDLK_LEFT:
                if (menu->choicePseudo || menu->caserne) {
                    if (menu->cursorX == 9.5 * TAILLE_BLOC) {
                        menu->cursorX += (2 * 7.5 * TAILLE_BLOC);
                        menu->idImgPseudo += 2;
                    }
                    else {
                        menu->cursorX -= (7.5 * TAILLE_BLOC);
                        menu->idImgPseudo--;
                    }
                }
                if (menu->auberge) {
                    if (menu->cursorX == 11 * TAILLE_BLOC) {
                        menu->cursorX += 9 * TAILLE_BLOC;
                        menu->idLevelUp++;
                    }
                    else {
                        menu->cursorX -= 9 * TAILLE_BLOC;
                        menu->idLevelUp--;
                    }
                }
                break;

            case SDLK_RIGHT:
                if (menu->choicePseudo || menu->caserne) {
                    menu->cursorX %= (int)(3 * (7.5 * TAILLE_BLOC));
                    menu->cursorX += 7.5 * TAILLE_BLOC;
                    menu->idImgPseudo %= 3;
                    if (menu->cursorY == 6.5 * TAILLE_BLOC)
                        menu->idImgPseudo++;
                    else
                        menu->idImgPseudo += 4;
                    
                }
                if (menu->auberge) {
                    if (menu->cursorX == 11 * TAILLE_BLOC) {
                        menu->cursorX += 9 * TAILLE_BLOC;
                        menu->idLevelUp++;

                    }
                    else {
                        menu->cursorX -= 9 * TAILLE_BLOC;
                        menu->idLevelUp--;
                    }
                }
                break;

               

            case SDLK_RETURN:  // RETURN = Touche Enter du clavier
                

                if (menu->caserne) {
                    menu->idAchat = menu->idImgPseudo - 1;

                    int price = game->priceUnit[menu->idAchat];
                    std::cout << price << std::endl;
                    std::cout << game->playerBlue->money << std::endl;

                    if (game->idTurn == "B") {
                        if (price <= game->playerBlue->money) {
                            game->modeAchat = true;
                            game->playerBlue->money -= price;
                            std::string money = std::to_string(game->playerBlue->money);
                            loadFont(money.c_str(), 24, "N", hud->money);
                        }
                    }
                    else {
                        if (price <= game->playerRed->money) {
                            game->modeAchat = true;
                            game->playerRed->money -= price;
                            std::string money = std::to_string(game->playerRed->money);
                            loadFont(money.c_str(), 24, "N", hud->money);
                        }
                    }
                    menu->reloadMenu = true;
                }
               

                if (menu->choiceNbPlayers) {
                    if (menu->cursorY == 350) {
                        game->nbPlayers = 1; // 1 Joueur + IA
                        game->IA = true;
                    }
                    else   
                        game->nbPlayers = 2; // 2 Joueurs
                    
                    menu->choiceNbPlayers = false;
                    menu->choicePseudo = true;
                    menu->cursorX = 9.5 * TAILLE_BLOC;
                    menu->cursorY = 6.5 * TAILLE_BLOC;
                }

                else if (menu->choicePseudo) {
                    std::string pathImgPseudo = "img/HUD/" + std::to_string(menu->idImgPseudo) + game->idTurn + ".png";
                    if (game->nbPlayers == 1) {
                        loadTexture(pathImgPseudo.c_str(), hud->pannelPlayerBlue);
                        //menu->choicePseudo = false;
                        game->playerRed->name = nameOfIA(game->playerBlue->name);
                        int randImg = rand() % 6 + 1;
                        std::string pathImgIA = "img/HUD/" + std::to_string(randImg) + "R.png";
                        std::cout << "PSEUDO IA " << pathImgIA << std::endl;
                        loadTexture(pathImgIA.c_str(), hud->pannelPlayerRed);
                    }
                    else {
                        if (game->idTurn == "B") {
                            loadTexture(pathImgPseudo.c_str(), hud->pannelPlayerBlue);
                            menu->cursorX = 9.5 * TAILLE_BLOC;
                            menu->cursorY = 6.5 * TAILLE_BLOC;
                            menu->idImgPseudo = 1;
                        }
                        else {
                            loadTexture(pathImgPseudo.c_str(), hud->pannelPlayerRed);
                            menu->cursorX = 9.5 * TAILLE_BLOC;
                            menu->cursorY = 6.5 * TAILLE_BLOC;
                            menu->idImgPseudo = 1;
                        }
                            
                    }

                }
                else if (game->modeTuto) {
                    game->nbTuto++;
                    if (game->nbTuto == 5) {
                        game->modeTuto = false;
                    }
                }
                else if (game->dayCount == 0) { //Enlever l'�cran d'histoire
                    game->dayCount++;
                    game->changeDay = true;
                }
                else if (menu->auberge) {
                    menu->reloadMenu = true;

                    int price;
                    if (game->idTurn == "B") {
                        price = game->priceUnit[menu->idLevelUp] * (game->playerBlue->arrayLevels[menu->idLevelUp] + 1) / 2;
                        if (price <= game->playerBlue->money) {
                            levelUp(units, game->idTurn, menu->idLevelUp);
                            game->playerBlue->arrayLevels[menu->idLevelUp]++;
                            std::cout << menu->idLevelUp << " a level up " << game->playerBlue->arrayLevels[menu->idLevelUp] << std::endl;
                            game->playerBlue->money -= price;
                            std::string money = std::to_string(game->playerBlue->money);
                            loadFont(money.c_str(), 24, "N", hud->money);

                        }
                    }
                    else {
                        price = game->priceUnit[menu->idLevelUp] * (game->playerRed->arrayLevels[menu->idLevelUp] + 1) / 2;
                        if (price <= game->playerRed->money) {
                            levelUp(units, game->idTurn, menu->idLevelUp);
                            game->playerRed->arrayLevels[menu->idLevelUp]++;
                            game->playerRed->money -= price;
                            std::string money = std::to_string(game->playerRed->money);
                            loadFont(money.c_str(), 24, "N", hud->money);

                        }
                    }

                }
                else if (game->endGame) {
                    game->endGame = false;
                    menu->choiceNbPlayers = true;
                    menu->inputTextInFocus = true;
                    menu->allowed_to_type_again = true;
                    reInitGame(game, hud, menu, playerIA, units, inputPseudo, path, rangeMove, rangeFight, arrayObs, rangeFightBlue, arrayBonus);
                    loadTexutresHUD(hud, game);
                }
                
                break;


            case SDLK_q:
                if (menu->auberge) {
                    menu->auberge = false;
                    menu->idLevelUp = 0;
                }else if (menu->caserne) {
                    menu->caserne = false;
                    menu->idAchat = 0;
                }
                break;

            case SDLK_SPACE:
                if (!game->changeDay)
                    game->countActions = 3;
                break;
            }
            




                // END TOUCHES CLAVIER


        case SDL_KEYUP:
            if (menu->choicePseudo) {
                if (menu->inputTextInFocus && inputPseudo.length() > 0 && e.key.keysym.sym == SDLK_RETURN) {

                    if (game->idTurn == "B") {
                        game->playerBlue->name = inputPseudo;
                        std::cout << "WRITE BLUE " << game->playerBlue->name << std::endl;
                        loadFont(game->playerBlue->name.c_str(), 28, "N", hud->pseudo);
                    }
                    else {
                        game->playerRed->name = inputPseudo.c_str();
                    }


                    if (game->nbPlayers == 1) {
                        menu->inputTextInFocus = false;
                        menu->allowed_to_type_again = false;
                        menu->choicePseudo = false;
                        game->modeTuto = true;
                    }
                    else {
                        if (game->idTurn == "B") {
                            menu->inputTextInFocus = true;
                            menu->allowed_to_type_again = true;
                            game->idTurn = "R";
                            inputPseudo = "";
                            std::cout << "WRITE BLUE 2" << game->playerBlue->name << std::endl;
                        }
                        else {
                            menu->inputTextInFocus = false;
                            menu->allowed_to_type_again = false;
                            game->idTurn = "B";
                            menu->choicePseudo = false;
                            game->modeTuto = true;
                        }
                    }
                }

                if (!menu->allowed_to_type_again && inputPseudo.length() < 10 && menu->inputTextInFocus) {
                    menu->allowed_to_type_again = true;
                }
            }
            break;


        default:
            break;
        }
    }
}

bool isAuberge(SDL_Event e) {
    if ((e.button.x >= TAILLE_BLOC && e.button.x <= 4 * TAILLE_BLOC && e.button.y >= 3 * TAILLE_BLOC && e.button.y <= 7 * TAILLE_BLOC)
        || (e.button.x >= 35 * TAILLE_BLOC && e.button.x <= 38 * TAILLE_BLOC && e.button.y >= 13 * TAILLE_BLOC && e.button.y <= 17 * TAILLE_BLOC))
        return true;
    else
        return false;
}


bool isCaserne(SDL_Event e) {
    //std::cout << "click on " << e.button.x << ", " << e.button.y << std::endl;

    if ( (e.button.x >= 7 * TAILLE_BLOC && e.button.x <= 11 * TAILLE_BLOC && e.button.y >= 0 && e.button.y <= 3 * TAILLE_BLOC)
        || ( e.button.x >= 30 * TAILLE_BLOC && e.button.x <= 34 * TAILLE_BLOC && e.button.y >= 14 * TAILLE_BLOC && e.button.y <= 17 * TAILLE_BLOC) )
        return true;
    else
        return false;
}
