#include "mouvement.h"
#include "heap.h"
#include "constantes.h"
#include "units.h"
#include "load.h"


#include <algorithm>
#include <vector>
#include <iostream>
#include "SDL/SDL.h"
#include "GL/glew.h"


void initNode(Cell* node, int x, int y, int G, int H, int F, Cell* parent) {
	node->x = x;
	node->y = y;
	node->G = G;
	node->H = H;
	node->F = F;
	node->parent = parent;
}

Cell* createCellAStar(int x, int y, int G, int H, int F, Cell* parent) {
	Cell* node = (Cell*)malloc(sizeof(Cell));
	node->x = x;
	node->y = y;
	node->G = G;
	node->H = H;
	node->F = F;
	node->parent = parent;

	return node;
}

Cell* createCell(int x, int y) {
	Cell* cell = (Cell*)malloc(sizeof(Cell));
	cell->x = x;
	cell->y = y;

	return cell;
}


void initCell(int x, int y, Cell* c, bool walkable) {
	c->x = x;
	c->y = y;
	c->walkable = walkable;
}


bool isInside(Cell cell, std::vector<Cell> array) {
	for (int i = 0; i < array.size(); i++) {
		if (array[i].x == cell.x && array[i].y == cell.y)
			return true;
	}

	return false;
}


int isInsideIndex(Cell cell, std::vector<Cell> array) {
	for (int i = 0; i < array.size(); i++) {
		if (array[i].x == cell.x && array[i].y == cell.y)
			return i;
	}
	return -1;
}

bool isSameCell(Cell c1, Cell c2) {
	if (c1.x == c2.x && c1.y == c2.y)
		return true;
	else
		return false;
}


int H_Score(Cell cell, Cell end) {
	int x = abs(end.x - cell.x) / TAILLE_BLOC;
	int y = abs(end.y - cell.y) / TAILLE_BLOC;

	return x + y;
}


bool isInGrid(Cell cell) {
	if (cell.x >= 0 && cell.x < 1280 && cell.y < 736 && cell.y >= 0)
		return true;
	else
		return false;
}



int searchIndexMin(std::vector<float> const array) {
	if (array.empty())
		return -1;
	else {
		int minimum = array[0];
		int indexMin = 0;
		for (int i = 1; i < array.size(); i++) {
			if (array[i] < minimum) {
				indexMin = i;
			}
		}
		return indexMin;
	}
}


int A_Star(Cell start, Cell end, std::vector<Cell> const arrayObstacle, std::vector<Cell>& path, bool savePath) {
	// Listes des cases � visiter
	BHeap openList;

	// Listes des cases visit�es
	BHeap closedList;

	// On commence � la case d�part
	start.G = 0;
	start.F = 0;

	start.parent = NULL;
	Cell* current = &start;

	// On inclue la case d�part dans la Open List
	openList.Insert(start);

	int alreadyOnOpenList = 0;

	while (openList.Size() > 0) {

		// New current is the Node with lowest F Cost
		Cell* newCase = createCellAStar(openList.ExtractMin()->x, openList.ExtractMin()->y,
			openList.ExtractMin()->G, openList.ExtractMin()->H, openList.ExtractMin()->F,
			openList.ExtractMin()->parent);

		current = newCase;

		// Drop Current Node from the openList
		openList.DeleteMin();

		// Add Current Node to the closedList
		closedList.Insert(*current);

		if (isSameCell(*current, end)) {
			int cost = 0;
			while (!isSameCell(*current, start)) {
				cost++;
				if (savePath)
					path.push_back(*current);
				current = current->parent;

			}
			return cost;
		}

		// Creation des huits voisins lat�raux de current
		Cell west, east, north, south, northWest, northEast, southWest, southEast;
		initNode(&west, current->x + TAILLE_BLOC, current->y, 10, 0, 10, NULL);
		initNode(&east, current->x - TAILLE_BLOC, current->y, 10, 0, 10, NULL);
		initNode(&north, current->x, current->y - TAILLE_BLOC, 10, 0, 10, NULL);
		initNode(&south, current->x, current->y + TAILLE_BLOC, 10, 0, 10, NULL);
		initNode(&northWest, current->x - TAILLE_BLOC, current->y - TAILLE_BLOC, 14, 0, 14, NULL);
		initNode(&northEast, current->x + TAILLE_BLOC, current->y - TAILLE_BLOC, 14, 0, 14, NULL);
		initNode(&southWest, current->x - TAILLE_BLOC, current->y + TAILLE_BLOC, 14, 0, 14, NULL);
		initNode(&southEast, current->x + TAILLE_BLOC, current->y + TAILLE_BLOC, 14, 0, 14, NULL);

		Cell siblings[8] = { west, east, north, south, northWest, northEast, southWest, southEast };

		// Validit� des cases voisines
		for (int i = 0; i < 8; i++) {

			if (!savePath && !isInside(siblings[i], path)) {
				continue;
			}

			if (!isInGrid(siblings[i]) || closedList.heapSearch(siblings[i], 0) || isInside(siblings[i], arrayObstacle)) {
				continue;
			}

			alreadyOnOpenList = openList.heapSearchIndex(siblings[i], 0);
			// Already in Open List
			if (alreadyOnOpenList != -1) {
				// If path is lowest by this way (using current node)
				if (siblings[i].G + current->G < openList.value(alreadyOnOpenList)->G) {
					openList.value(alreadyOnOpenList)->parent = current;
					openList.value(alreadyOnOpenList)->G = siblings[i].G + current->G;
					openList.value(alreadyOnOpenList)->F = openList.value(alreadyOnOpenList)->G + openList.value(alreadyOnOpenList)->H;
				}
			}
			// Add onto Open List
			else {
				siblings[i].parent = current;
				siblings[i].G += siblings[i].parent->G;
				siblings[i].H = H_Score(siblings[i], end);
				siblings[i].F = siblings[i].G + siblings[i].H;
				openList.Insert(siblings[i]);
			}

		}
	}
	// END WHILE LOOP
	return -1;

}
// END A_STAR FUNCTION


/* Cr�ation des diff�rents pattern de range pour les types d'unit� */
void patternCross(Cell startNode, std::vector<Cell>& range, std::vector<Cell> const arrayObs, int rangeMax) {
	for (int i = -rangeMax; i <= rangeMax; i++) {
		Cell* cellRange = createCell(startNode.x + TAILLE_BLOC * i, startNode.y);
		if (i != 0 && !isInside(*cellRange, arrayObs))
			range.push_back(*cellRange);
		else
			free(cellRange);
	}

	for (int i = -rangeMax; i <= rangeMax; i++) {
		Cell* cellRange = createCell(startNode.x, startNode.y + TAILLE_BLOC * i);
		if (i != 0 && !isInside(*cellRange, arrayObs))
			range.push_back(*cellRange);
		else
			free(cellRange);
	}
}
void patternStar(Cell startNode, std::vector<Cell>& range, std::vector<Cell> const arrayObs, int rangeMax) {
	for (int i = -rangeMax; i <= rangeMax; i++) {
		int casesY = rangeMax - abs(i);
		for (int j = -casesY; j <= casesY; j++) {
			Cell* cellRange = createCell(startNode.x + TAILLE_BLOC * i, startNode.y + TAILLE_BLOC * j);
			if ((j == 0 && i == 0) || isInside(*cellRange, arrayObs)) {
				free(cellRange);
				continue;
			}
			else {
				range.push_back(*cellRange);
			}
		}
	}
}
void patternStarDamier(Cell startNode, std::vector<Cell>& range, std::vector<Cell>& arrayObs, int rangeMax) {

	for (int i = -rangeMax; i <= rangeMax; i++) {
		int casesY = rangeMax - abs(i);
		
		for (int j = -casesY; j <= casesY; j += 2) {
			Cell* cellRange = createCell(startNode.x + TAILLE_BLOC * i, startNode.y + TAILLE_BLOC * j);
			
			if (isInside(*cellRange, arrayObs)) {
				free(cellRange);
				continue;				
			}
			else {
				range.push_back(*cellRange);
			}
		}
	}
}

void createRanges(Cell startNode, std::vector<Cell>& rangeMove, std::vector<Cell>& rangeFight, std::vector<Cell> arrayObs, Unit unit, std::vector<Unit*> units, Game* game, bool onlyFight) {
	std::vector<Cell> rangeMovePattern;
	std::vector<Cell> rangeFightPattern;
	
	
	if (unit.typeRange == "cross") {
		patternCross(startNode, rangeMovePattern, arrayObs, unit.rangeMax);
		removeEnnemyFromObstacles(units, arrayObs, game);
		patternCross(startNode, rangeFightPattern, arrayObs, unit.rangeMax / 2);
	}
	else if (unit.typeRange == "damier") {
		patternStarDamier(startNode, rangeMovePattern, arrayObs, unit.rangeMax);
		removeEnnemyFromObstacles(units, arrayObs, game);
		patternStarDamier(startNode, rangeFightPattern, arrayObs, unit.rangeMax);
	}
	else  {
		patternStar(startNode, rangeMovePattern, arrayObs, unit.rangeMax);
		removeEnnemyFromObstacles(units, arrayObs, game);
		patternStar(startNode, rangeFightPattern, arrayObs, unit.rangeMax / 2);
	}


	for (int i = 0; i < rangeFightPattern.size(); i++) {
		int cost = A_Star(startNode, rangeFightPattern[i], arrayObs, rangeFightPattern, false);
		if (cost > 0 && cost <= unit.rangeMax / 2) {
			rangeFight.push_back(rangeFightPattern[i]);
		}		
	}

	if (!onlyFight) {
		for (int i = 0; i < rangeMovePattern.size(); i++) {
			int cost = A_Star(startNode, rangeMovePattern[i], arrayObs, rangeMovePattern, false);
			if (cost >= unit.rangeMax / 2 && cost <= unit.rangeMax) {
				rangeMove.push_back(rangeMovePattern[i]);
			}
		}
	}
	
	
	// R�int�gre les unit�s dans les obstacles
	updateObstacles(units, arrayObs, game);
	
}







