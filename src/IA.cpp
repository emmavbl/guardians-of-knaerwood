#include "IA.h"

#include <iostream>
#include <string>
#include <vector>

std::string nameOfIA(std::string nameBlue) {
	std::string arrayNames[41] = { "Achraf", "Alaric", "Alexandre", "Alois", "Baptiste", "Benjamin", "Brian", "Charlotte", 
									"Cloe", "Clothilde", "Elo", "Emma", "Erwann", "Esther", "EvanJames", "Fabian", "Fanny"
									"Felix", "Amelie", "Gregoire", "Jeanne", "Judi", "Justine", "Keziah", "Laurine", "Lea", 
									"Loona", "Luc", "Ludwig", "Matteo", "Maxime", "Natacha", "Nelly", "Nils", "Paul", "Simon",
									"Sterenn", "Theo", "Thomas", "Daphne", "Vincent", "Zoe"};

	int randIndex = rand() % 41;

	while (arrayNames[randIndex] == nameBlue) {
		randIndex = rand() % 41;
	}

	return arrayNames[randIndex];
}

void createHeroesOfIA(IA* playerIA, Game* game, std::vector<Unit*> units, Menu* menu) {
    game->modeAchat = true;

    switch (playerIA->randomEquipe)
    {
    // Equipe IA n1 : 1 Orc + 3 Guerriers
    case 0:
        if (playerIA->nbUnits == 0) {
            menu->idAchat = 5;
        }
        else if (playerIA->nbUnits == 1) {
            menu->idAchat = 4;
        }
        else if (playerIA->nbUnits == 2) {
            menu->idAchat = 4;
        }
        else if (playerIA->nbUnits == 3) {
            menu->idAchat = 4;
            playerIA->equipeCreee = true;
        }
        break;
    
    // Equipe IA n2 : 2 Orcs
    case 1:
        if (playerIA->nbUnits == 0) {
            menu->idAchat = 5;
        }
        else if (playerIA->nbUnits == 1) {
            menu->idAchat = 5;
            playerIA->equipeCreee = true;
        }
        break;

    // Equipe IA n1 : 6 Guerriers
    case 2:
        if (playerIA->nbUnits == 0) {
            menu->idAchat = 4;
        }
        else if (playerIA->nbUnits == 1) {
            menu->idAchat = 4;
        }
        else if (playerIA->nbUnits == 2) {
            menu->idAchat = 4;
        }
        else if (playerIA->nbUnits == 3) {
            menu->idAchat = 4;
        }
        else if (playerIA->nbUnits == 4) {
            menu->idAchat = 4;
        }
        else if (playerIA->nbUnits == 5) {
            menu->idAchat = 4;
            playerIA->equipeCreee = true;
        }
        break;
    default:
        break;
    }



}
// ALGO D'ACHAT D'UNITES POUR L'IA
/*
if (playerIA->nextAchat == NUll)
    playerIA->nextAchat = rand type unit

if (playerIA.money = type.achat.money && level[type] > 2)
    modeAchat = true;
    id achat

if (playerIA.money = type.lvl.money && )
    levelup

*/
void manageMoneyOfIA(Game* game, std::vector<Unit*> units, Menu* menu) {

}



void IAalgo(Game* game, std::vector<Unit*> units, Bonus* arrayBonus[], std::vector<Cell> arrayObs, IA* playerIA, std::vector<Cell>& rangeMove, std::vector<Cell>& rangeFight, std::vector<Cell>& path, std::vector<Cell>& rangeFightBlue) {

    Cell cellUnit;
    Cell cellTarget;
    Cell cellTargetTmp;
    Cell cellTargetBonus;
    initCell(-1, -1, &cellTargetBonus, true);
    int cost = 100;
    playerIA->targetIsBonus = false;

    std::vector<Cell> pathTmp;

    path.clear();
    rangeFight.clear();
    rangeMove.clear();
    rangeFightBlue.clear();

    // Boucle sur les units IA avec prio <= 3
    for (int i = 0; i < units.size(); i++)
    {
        if (units[i]->playerID == "B" || units[i]->priorite > 4)
            continue;

        // Coordonn�es de la case de l'unit� IA
        cellUnit.x = units[i]->x;
        cellUnit.y = units[i]->y;

        // Si l'unit� n'a pas de target, attribuer une target random dans blue
        if (units[i]->target == NULL || units[i]->target->pv <= 0) {
            int randIndex = rand() % units.size();
            while (units[randIndex]->playerID != "B") {
                randIndex = rand() % units.size();
            }
            units[i]->target = units[randIndex];
            units[i]->targetID = randIndex;
        }

        // Coordonn�es de la case target
        cellTarget.x = units[i]->target->x;
        cellTarget.y = units[i]->target->y;

        // Cout entre l'unit� et la target actuelle
        removeOneUnitFromObstacles(units, units[i]->targetID, arrayObs, game);
        if (isInside(cellTarget, arrayObs)) {
        }
        int costActuelle = A_Star(cellUnit, cellTarget, arrayObs, path, true);

        // Si une target est plus proche, on change donc de target
        for (int j = 0; j < units.size(); j++)
        {
            if (units[j]->playerID == "R")
                continue;

            cellTargetTmp.x = units[j]->x;
            cellTargetTmp.y = units[j]->y;

            pathTmp.clear();
            removeOneUnitFromObstacles(units, j, arrayObs, game);
            cost = A_Star(cellUnit, cellTargetTmp, arrayObs, pathTmp, true);

            if (cost != -1 && cost <= costActuelle) {
                path.clear();
                path = pathTmp;
                units[i]->target = units[j];
                units[i]->targetID = j;
                costActuelle = cost;
            }
        }

        // LES BONUS FONT BUGUER l'IA, NOUS AVONS DECIDER DE LES ENLEVER DES TARGETS DISPONIBLES

        //// Si une target est plus proche, on change donc de target (Pour les bonus)
        //for (int k = 0; k < 4; k++)
        //{
        //    if (arrayBonus[k]->caseX == -1)
        //        continue;

        //    cellTargetTmp.x = arrayBonus[k]->caseX;
        //    cellTargetTmp.y = arrayBonus[k]->caseY;
        //    pathTmp.clear();
        //    cost = A_Star(cellUnit, cellTargetTmp, arrayObs, pathTmp, true);

        //    if (cost != -1 && cost < costActuelle) {
        //        path.clear();
        //        path = pathTmp;
        //        cellTargetBonus.x = cellTargetTmp.x;
        //        cellTargetBonus.y = cellTargetTmp.y;
        //        costActuelle = cost;
        //        std::cout << "SHORTER" << costActuelle << std::endl;
        //    }
        //}


        // Cr�ation des ranges de l'unit�
        rangeMove.clear();
        rangeFight.clear();
        createRanges(cellUnit, rangeMove, rangeFight, arrayObs, *units[i], units, game, false);

        // Ordre de priorit� en fonction des localisations dans les ranges
        if (isInside(cellTarget, rangeFight)) {     // Unite IA dans la Range Fight de Target
            units[i]->priorite = 1;
        }
        else if (isInside(cellTarget, rangeMove)) {     // Unite IA dans la Range Move de Target
            units[i]->priorite = 2;
        }
        else if ((isInside(cellTargetBonus, rangeFight) || isInside(cellTargetBonus, rangeMove))) {    // Bonus dans les ranges de l'IA
            units[i]->priorite = 3;
        }
        else if (!isInside(cellTarget, rangeMove)) {        // L'IA n'est pas dans les ranges de la Target
            units[i]->priorite = 4;
        }
        else {
            units[i]->priorite = 5;
        }
    }


    // Choisir l'unit� avec l'ordre de priorit� la plus basse
    int prioMin = 4;
    for (int m = 0; m < units.size(); m++)
    {
        if (units[m]->playerID == "B")
            continue;

        if (units[m]->priorite <= prioMin) {
            prioMin = units[m]->priorite;
            game->idSelect = m;
        }
    }

    Cell cellUnitSelected;
    cellUnitSelected.x = units[game->idSelect]->x;
    cellUnitSelected.y = units[game->idSelect]->y;
    rangeMove.clear();
    rangeFight.clear();
    createRanges(cellUnitSelected, rangeMove, rangeFight, arrayObs, *units[game->idSelect], units, game, false);


    switch (prioMin)
    {
    case 1: // Fight
        game->modeFight = true;
        game->idCible = units[game->idSelect]->targetID;
        break;

    case 2: // Move in RangeFight de l'ennemi
        game->modeMove = true;
        game->moveX = units[game->idSelect]->target->x;
        game->moveY = units[game->idSelect]->target->y;
        game->indexPathMove = path.size() - 1;
        rangeFightBlue.clear();
        Cell cellTargetSelected;
        cellTargetSelected.x = units[game->idSelect]->target->x;
        cellTargetSelected.y = units[game->idSelect]->target->y;
        createRanges(cellTargetSelected, rangeFightBlue, rangeFightBlue, arrayObs, *units[game->idSelect]->target, units, game, true);
        break;

    case 3: // Move on Bonus
        units[game->idSelect]->target = NULL;
        playerIA->targetIsBonus = true;
        game->modeMove = true;
        game->moveX = cellTargetBonus.x;
        game->moveY = cellTargetBonus.y;
        game->indexPathMove = path.size() - 1;
        units[game->idSelect]->priorite = 5;
        break;

    case 4: // Move vers Ennemy
        game->modeMove = true;
        game->moveX = units[game->idSelect]->target->x;
        game->moveY = units[game->idSelect]->target->y;
        game->indexPathMove = path.size() - 1;
        units[game->idSelect]->priorite = 5;
        break;

    default:
        units[game->idSelect]->priorite = 5;
        break;
    }

    // Si toutes les prioMin sont � 5, on met la derni�re � 4
    prioMin = 5;
    for (int m = 0; m < units.size(); m++)
    {
        if (units[m]->playerID == "B")
            continue;

        if (units[m]->priorite <= prioMin) {
            prioMin = units[m]->priorite;
        }
        if (m == units.size() - 1 && prioMin == 5) {
            units[m]->priorite = 4;
        }
    }

    playerIA->turn = false;
}

void IAturn(Game* game, std::vector<Unit*> units, Bonus* arrayBonus[], std::vector<Cell> arrayObs, IA* playerIA, std::vector<Cell>& rangeMove, std::vector<Cell>& rangeFight, Menu* menu, std::vector<Cell>& path, std::vector<Cell>& rangeFightBlue) {

    // Jour 1 --> Cr�ation de l'equipe
    if (game->dayCount == 2 && !playerIA->equipeCreee)
        createHeroesOfIA(playerIA, game, units, menu);
    // Jour n + 1 --> Achat et Level Up
    else
        manageMoneyOfIA(game, units, menu);

    // Lorsque l'equipe est cr�e, IA turn
    if (playerIA->equipeCreee) {
        IAalgo(game, units, arrayBonus, arrayObs, playerIA, rangeMove, rangeFight, path, rangeFightBlue);
    }

}

