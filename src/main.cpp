#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include "GL/glew.h"
#include "GL/glut.h"

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <ctime>

#include "constantes.h"
#include "load.h"
#include "init.h"
#include "display.h"
#include "units.h"
#include "game.h"
#include "events.h"
#include "mouvement.h"
#include "IA.h"



int main(int argc, char** argv)
{
    srand(time(NULL));

    // Création de la Fenêtre + context Opengl
    SDL_Window* window = NULL;
    SDL_GLContext glcontext = NULL;

    // Initialisation SDL / Opengl
    int init = initialisation(&window, &glcontext);
    if (init != 0) {
        std::cout << "Erreur d'initialisation, fermeture du programme." << std::endl;
        return 1;
    }

    /* INITIALISATION POUR UNE PARTIE DE JEU */
    // Initialisation Game + Hud + Menu
    Game* game = initGame();
    Hud* hud = initHud();
    Menu* menu = initMenu();
    // Initialisation des Bonus
    Bonus* arrayBonus[4];
    createAllBonus(arrayBonus);
    // Initialisation des deux Joueurs
    int array[6] = { 1 };
    game->playerBlue = new Player("B", "Thomas", 200, 3, array);
    game->playerRed = new Player("R", "Emma", 200, 3, array);
    initPlayer(game->playerBlue, "B", "Thomas");
    initPlayer(game->playerRed, "R", "Emma");
    // Tableau des unités de jeu
    std::vector<Unit*> units;
    // Initialisation de l'IA
    IA* playerIA = initIA();

    // Cursor
    GLuint cursorTexture[3];
    loadTexture("img/HUD/cursor.png", hud->cursorTexture);
    SDL_ShowCursor(SDL_DISABLE);

    // Textures Tuto
    GLuint tuto1[3];
    loadTexture("img/tuto/tuto1.png", tuto1);
    GLuint tuto2[3];
    loadTexture("img/tuto/tuto2.png", tuto2);
    GLuint tuto3[3];
    loadTexture("img/tuto/tuto3.png", tuto3);
    GLuint tuto4[3];
    loadTexture("img/tuto/tuto4.png", tuto4);
    
    // Textures Menu
    GLuint menuBegin[3]; // 1 Joueur ou 2
    loadTexture("img/menu/menuBegin.png", menuBegin);
    GLuint cursorMenu[3];
    loadTexture("img/HUD/menuCursor.png", cursorMenu);
    GLuint menuPseudoBlue[3];
    loadTexture("img/menu/menuPseudoBlue.png", menuPseudoBlue);
    GLuint menuPseudoRed[3];
    loadTexture("img/menu/menuPseudoRed.png", menuPseudoRed);
    GLuint cadreSelection[3];
    loadTexture("img/menu/cadreSelection.png", cadreSelection);
    GLuint auberge[3];
    loadTexture("img/menu/auberge.png", auberge);
    GLuint caserne[3];
    loadTexture("img/menu/caserne.png", caserne);

    // Textures de la carte
    GLuint Carte[3];
    loadTexture("img/map/carte.png", Carte);
    GLuint Grille[3];
    loadTexture("img/map/grille.png", Grille);

    // Textures des personnages
    GLuint textureWitch[3];
    loadTexture("img/Units/witch.png", textureWitch);
    GLuint textureFee[3];
    loadTexture("img/Units/fee.png", textureFee);
    GLuint textureFarfadet[3];
    loadTexture("img/Units/lutin.png", textureFarfadet);
    GLuint textureDemon[3];
    loadTexture("img/Units/imp.png", textureDemon);
    GLuint textureGladiator[3];
    loadTexture("img/Units/gladiator.png", textureGladiator);
    GLuint textureOrc[3];
    loadTexture("img/Units/orc.png", textureOrc);    

    // Textures + Font HUD
    GLuint rangeTexture[3];
    GLuint texturePV[3];
    GLuint pressEnter[3];
    loadTexutresHUD(hud, game);
    loadTexture("img/Units/rangeSelector5.png", rangeTexture);
    loadFont("test", 16, "R", texturePV);
    loadFont("appuyez sur enter pour terminer", 24, "W", pressEnter);

    // === Variables de jeu === //
    // Compteur de frames pour l'affichage du Changement de Jour
    int timeCountDay = 0;
    // Input Pseudo
    std::string inputPseudo = "";
    loadFont(inputPseudo.c_str(), 26, "N", menu->inputTexturePseudo);
    // 1 : Le jeu tourne | 0 : On quitte le jeu
    int runGame = 1;
    //Tableau de texture pour le menu level up / Auberge 
    GLuint prixLevel[6][3];
    GLuint level[6][3];
    GLuint playerMoney[3];
    // Variable de fin de jeu
    std::string textEndgame;
    const char* colorEndgame = "B";
    GLuint endGameText[3];
    bool firstFrameEndgame = true; // Compteur de frames pour eviter le reload de Endgame
    // Variables A Star
    std::vector<Cell> path;
    std::vector<Cell> rangeMove;
    std::vector<Cell> rangeFight;
    std::vector<Cell> arrayObs;
    std::vector<Cell> rangeFightBlue;

    // Création des obstalces de la map
    createObstacles(arrayObs);

    // Mis en place des Bonus sur la map
    updateBonus(arrayBonus, arrayObs);

    while (runGame)
    {
        /* Recuperation du temps au debut de la boucle */
        int startTime = SDL_GetTicks();
        
        // Seconds --> 10 frames par secondes
        int seconds = SDL_GetTicks() / 100;

        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(0.0, 0.0, 0.0, 0.0);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        // Activation du Texturing
        glEnable(GL_TEXTURE_2D);

        // Activation de la transparence
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



        // DISPLAY THE GAME
        if (menu->choiceNbPlayers) {    // Menu 1 joueur ou 2 
            displayTexture(menuBegin, 0, 0, 0, 0, menuBegin[1], menuBegin[2]);
            displayTexture(cursorMenu, 0, 0, menu->cursorX, menu->cursorY, cursorMenu[1], cursorMenu[2]);
        }
        else if (menu->choicePseudo)    // Menu choix du Hero + pseudo
        {
            if (game->idTurn == "B")
                displayTexture(menuPseudoBlue, 0, 0, 0, 0, menuPseudoBlue[1], menuPseudoBlue[2]);
            else
                displayTexture(menuPseudoRed, 0, 0, 0, 0, menuPseudoRed[1], menuPseudoRed[2]);
            
            if (inputPseudo != "")
                displayTexture(menu->inputTexturePseudo, 0, 0, 650, 112, menu->inputTexturePseudo[1], menu->inputTexturePseudo[2]);

            displayTexture(cadreSelection, 0, 0, menu->cursorX, menu->cursorY, cadreSelection[1], cadreSelection[2]);
        }
        else if (menu->auberge) { // menu de level up (Auberge)
            // Image Menu Auberge
            displayTexture(auberge, 0, 0, 0, 0, auberge[1], auberge[2]);
            // display Auberge 
            displayMenuAuberge(menu, game, prixLevel, level, playerMoney);
            // Cursor Menu
            displayTexture(cursorMenu, 0, 0, menu->cursorX, menu->cursorY, cursorMenu[1], cursorMenu[2]);
        }
        else if (menu->caserne) {
            // Image Menu Caserne
            displayTexture(caserne, 0, 0, 0, 0, caserne[1], caserne[2]);
            // Fonction algo Caserne
            displayMenuCaserne(menu, game, playerMoney);
            // Cursor Menu
            displayTexture(cadreSelection, 0, 0, menu->cursorX, menu->cursorY, cadreSelection[1], cadreSelection[2]);
        }
        else   // Carte + Unité
        {   
            displayMapAndUnits(game, units, seconds, Carte, Grille);
            displayBonus(arrayBonus, arrayObs);

            // Affichage des tutoriels
            if (game->modeTuto) {
                if (game->nbTuto == 1)
                    displayTexture(tuto1, 0, 0, 0, WINDOW_HEIGHT - tuto1[2], tuto1[1], tuto1[2]);
                else if (game->nbTuto == 2)
                    displayTexture(tuto2, 0, 0, 0, WINDOW_HEIGHT - tuto2[2], tuto2[1], tuto2[2]);
                else if (game->nbTuto == 3)
                    displayTexture(tuto3, 0, 0, 0, WINDOW_HEIGHT - tuto3[2], tuto3[1], tuto3[2]);
                else
                    displayTexture(tuto4, 0, 0, 0, 0, tuto4[1], tuto4[2]);
            }

            // Affichage des différentes Ranges
            if (game->modeSelect && game->idSelect != -1 && units[game->idSelect]->playerID == game->idTurn) {
                for (int i = 0; i < rangeMove.size(); i++) {
                    displayTexture(rangeTexture, 0, 0, rangeMove[i].x, rangeMove[i].y, rangeTexture[1] / 3, rangeTexture[2]);
                }
                for (int i = 0; i < rangeFight.size(); i++) {
                    displayTexture(rangeTexture, TAILLE_BLOC, 0, rangeFight[i].x, rangeFight[i].y, rangeTexture[1] / 3, rangeTexture[2]);
                }
                for (int i = 0; i < path.size(); i++) {
                    displayTexture(rangeTexture, 2. * TAILLE_BLOC, 0, path[i].x, path[i].y, rangeTexture[1] / 3, rangeTexture[2]);
                }
            }

            // Si Fin du Jeu
            if ((game->playerBlue->nbUnits == 0 || game->playerRed->nbUnits == 0) && game->dayCount > 3) // test  la mort des unités
            {
                game->endGame = true;
                displayEndGame(game, textEndgame, colorEndgame, endGameText, firstFrameEndgame, pressEnter);
                timeCountDay = 0;
                
            } 
            else {
                displayHUD(game, hud, timeCountDay, seconds, units);
            }               
        }

        // Desactivation du Transparence + Texturing
        glDisable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);

        /* Echange du front et du back buffer : mise a jour de la fenetre */
        SDL_GL_SwapWindow(window);

        // Evenements ou le tour de l'IA
        if (game->idTurn == "B" || (!game->IA && game->idTurn == "R"))
            manageEvents(game, hud, menu, runGame, units, inputPseudo, rangeMove, rangeFight, arrayObs, path, playerIA, rangeFightBlue, arrayBonus);
        else if (game->IA && game->idTurn == "R" && playerIA->turn)
            IAturn(game, units, arrayBonus, arrayObs, playerIA, rangeMove, rangeFight, menu, path, rangeFightBlue);

        // Algo des différentes phases de Jeu
        gameModes(game, playerIA, units, hud, arrayObs, arrayBonus, path, rangeMove, rangeFight, rangeFightBlue);

        // Acheter des unités dans la caserne
        buyUnitInCaserne(game, menu, playerIA, arrayObs, units, texturePV, textureWitch, textureFee, textureFarfadet,
                        textureDemon, textureGladiator, textureOrc);
        

        /* Calcul du temps ecoule */
        int elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if (elapsedTime < FRAMERATE_MILLISECONDS)
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    // Suppression de la fenetre
    SDL_DestroyWindow(window);
    
    // Once finished with OpenGL functions, the SDL_GLContext can be deleted.
    SDL_GL_DeleteContext(glcontext);

    /* Liberation des ressources associees a la SDL */
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
     
    return EXIT_SUCCESS;
}
