#include "game.h"
#include "constantes.h"
#include "units.h"
#include "init.h"
#include "mouvement.h"
#include "IA.h"
#include "load.h"

#include "SDL/SDL.h"
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include "GL/glew.h"
#include "GL/glut.h"

#include <vector>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <string.h>





int initialisation(SDL_Window** window, SDL_GLContext* glcontext) {
    /* Initialisation de la SDL */
    if (-1 == SDL_Init(SDL_INIT_VIDEO))
    {
        //fprintf(stderr,"Impossible d'initialiser la SDL. Fin du programme.\n");
        std::cout << "Impossible d'initialiser la SDL. Fin du programme.\n";
        SDL_Quit();

        return 1;
    }

    // Initialisation SLD2_image
    IMG_Init(IMG_INIT_PNG);

    // Initialisation SLD2_TTF
    TTF_Init();
    SDL_StartTextInput();


    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */
    *window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (window == 0) {
        std::cout << "Erreur lors de la creation de la fenetre" << std::endl;
        SDL_Quit();

        return 1;
    }

    // Create an OpenGL context associated with the window.
    *glcontext = SDL_GL_CreateContext(*window);
    if (glcontext == 0) {
        std::cout << "Erreur lors de la creation du context OpenGL" << std::endl;
        SDL_Quit();

        return 1;
    }
  

    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (aspectRatio >= 1)
    {
        gluOrtho2D(
            -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio,
            -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
    }
    else
    {
        gluOrtho2D(
            -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
            -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
    }

	return 0;
}

Game* initGame() {
    Game* game = (Game*)malloc(sizeof(Game));

    game->playerBlue = NULL;
    game->playerRed = NULL;

    game->idTurn = "B";

    game->modeMove = false;
    game->modeFight = false;
    game->modeSelect = false;
    game->modeAchat = false;
    game->modeTuto = false;

    game->nbTuto = 1;

    game->textureChangeDay[3] = { 0 };
    game->dayCount = 0;
    game->changeDay = false;

    game->priceUnit[0] = 300;
    game->priceUnit[1] = 250;
    game->priceUnit[2] = 100;
    game->priceUnit[3] = 110;
    game->priceUnit[4] = 150;
    game->priceUnit[5] = 500;

    game->caseX = -1;
    game->caseY = -1;

    game->cursorX = -1;
    game->cursorY = -1;

    game->moveX = -1;
    game->moveY = -1;

    game->idCible = -1;
    game->idDescription = -1;
    game->idSelect = -1;

    game->endGame = false;

    game->startNode = (Cell*)malloc(sizeof(Cell));
    initCell(-1, -1, game->startNode, true);

    game->endNode = (Cell*)malloc(sizeof(Cell));
    initCell(-1, -1, game->startNode, true);

    game->indexPathMove = -1;
    
    game->countActions = 0;

    game->IA = false;
    
    return game;
}



Hud* initHud() {
    Hud* hud = (Hud*)malloc(sizeof(Hud));

    hud->cursorTexture[3] = { 0 };
    hud->cursorID = 3;

    hud->selector[3] = { 0 };
    
    // Panneau Description Unit�
    hud->pannelDescBlue[3] = { 0 };
    hud->pannelDescRed[3] = { 0 };
    hud->type[3] = { 0 };
    hud->lvl[3] = { 0 };
    hud->power[3] = { 0 };
    hud->story[3] = { 0 };
    hud->changeDescription = false;

    // Panneau Joueur 
    hud->pannelPlayerBlue[3] = { 0 };
    hud->pannelPlayerRed[3] = { 0 };
    hud->pseudo[3] = { 0 };
    hud->money[3] = { 0 };

    hud->side = "left";

    return hud;
}


Menu* initMenu() {
    Menu* menu = (Menu*)malloc(sizeof(Menu));

    menu->choiceNbPlayers = true;
    menu->choicePseudo = false;
    menu->auberge = false;
    menu->caserne = false;

    menu->reloadMenu = true;

    menu->cursorX = 350;
    menu->cursorY = 350;

    menu->idImgPseudo = 1;
    menu->idLevelUp = 0;
    menu->idAchat = 0;

    menu->allowed_to_type_again = true;

    menu->inputTextInFocus = true;

    menu->inputTexturePseudo[3] = { 0 };

    return menu;
}

void initBonus(Bonus * bonus, const char * type, int proba) {
    bonus->caseX = -1;
    bonus->caseY = -1;
    bonus->type = type;
    bonus->proba = proba;
    bonus->Texture[3] = { 0 };
}

void createAllBonus(Bonus* arrayBonus[]) {
    GLuint textureBonus[3];
    arrayBonus[0] = new Bonus(-1, -1, textureBonus, "cheese", 0);
    initBonus(arrayBonus[0], "cheese", 50);
    arrayBonus[1] = new Bonus(-1, -1, textureBonus, "chest", 0);
    initBonus(arrayBonus[1], "chest", 10);
    arrayBonus[2] = new Bonus(-1, -1, textureBonus, "coin", 0);
    initBonus(arrayBonus[2], "coin", 30);
    arrayBonus[3] = new Bonus(-1, -1, textureBonus, "fiole", 0);
    initBonus(arrayBonus[3], "fiole", 20);
    // Chargement des Textures des Bonus
    loadTexture("img/bonus/cheese.png", arrayBonus[0]->Texture);
    loadTexture("img/bonus/chest.png", arrayBonus[1]->Texture);
    loadTexture("img/bonus/coin.png", arrayBonus[2]->Texture);
    loadTexture("img/bonus/fiole.png", arrayBonus[3]->Texture);
}

IA* initIA() {
    IA* playerIA = (IA*)malloc(sizeof(IA));

    playerIA->minCost = 100;
   

    playerIA->randomEquipe = rand() % 3;
    playerIA->nbUnits = 0;
    playerIA->equipeCreee = false;
    playerIA->turn = true;
    playerIA->outRange = false;
    playerIA->targetIsBonus = false;

    return playerIA;
}

void loadTexutresHUD(Hud * hud, Game * game) {
    loadTexture("img/HUD/desc_B.png", hud->pannelDescBlue);
    loadTexture("img/HUD/desc_R.png", hud->pannelDescRed);
    loadTexture("img/HUD/selector.png", hud->selector);
    loadTexture("img/HUD/2B.png", hud->pannelPlayerBlue);
    loadTexture("img/HUD/2R.png", hud->pannelPlayerRed);
    loadTexture("img/menu/story.png", hud->story);
    // Fonts HUD
    loadFont("jour", 25, "R", game->textureChangeDay);
    loadFont("Type", 16, "W", hud->type);
    loadFont("Lvl", 16, "W", hud->lvl);
    loadFont("Pv", 16, "W", hud->pv);
    loadFont("Power", 16, "W", hud->power);
    loadFont(game->playerBlue->name.c_str(), 28, "N", hud->pseudo);
    std::string money = std::to_string(game->playerBlue->money);
    loadFont(money.c_str(), 24, "N", hud->money);
}

void reInitGame(Game * game) {
    game->playerBlue = NULL;
    game->playerRed = NULL;
    game->idTurn = "B";
    game->modeMove = false;
    game->modeFight = false;
    game->modeSelect = false;
    game->modeAchat = false;
    game->modeTuto = false;
    game->nbTuto = 1;
    game->textureChangeDay[3] = { 0 };
    game->dayCount = 0;
    game->changeDay = false;
    game->priceUnit[0] = 300;
    game->priceUnit[1] = 250;
    game->priceUnit[2] = 100;
    game->priceUnit[3] = 110;
    game->priceUnit[4] = 150;
    game->priceUnit[5] = 500;
    game->caseX = -1;
    game->caseY = -1;
    game->cursorX = -1;
    game->cursorY = -1;
    game->moveX = -1;
    game->moveY = -1;
    game->idCible = -1;
    game->idDescription = -1;
    game->idSelect = -1;
    game->endGame = false;
    game->startNode = (Cell*)malloc(sizeof(Cell));
    initCell(-1, -1, game->startNode, true);
    game->endNode = (Cell*)malloc(sizeof(Cell));
    initCell(-1, -1, game->startNode, true);
    game->indexPathMove = -1;
    game->countActions = 0;
    game->IA = false;
}



void reInitHud(Hud * hud) {
    hud->cursorTexture[3] = { 0 };
    hud->cursorID = 3;
    hud->selector[3] = { 0 };
    // Panneau Description Unit�
    hud->pannelDescBlue[3] = { 0 };
    hud->pannelDescRed[3] = { 0 };
    hud->type[3] = { 0 };
    hud->lvl[3] = { 0 };
    hud->power[3] = { 0 };
    hud->story[3] = { 0 };
    hud->changeDescription = false;
    // Panneau Joueur 
    hud->pannelPlayerBlue[3] = { 0 };
    hud->pannelPlayerRed[3] = { 0 };
    hud->pseudo[3] = { 0 };
    hud->money[3] = { 0 };
    hud->side = "left";
}


void reInitMenu(Menu * menu) {
    menu->choiceNbPlayers = true;
    menu->choicePseudo = false;
    menu->auberge = false;
    menu->caserne = false;
    menu->reloadMenu = true;
    menu->cursorX = 350;
    menu->cursorY = 350;
    menu->idImgPseudo = 1;
    menu->idLevelUp = 0;
    menu->idAchat = 0;
    menu->allowed_to_type_again = true;
    menu->inputTextInFocus = true;
    menu->inputTexturePseudo[3] = { 0 };
}

void reInitIA(IA * playerIA) {
    playerIA->minCost = 100;
    //playerIA->randomEquipe = rand() % 5;
    playerIA->randomEquipe = 0;
    playerIA->nbUnits = 0;
    playerIA->equipeCreee = false;
    playerIA->turn = true;
    playerIA->outRange = false;
    playerIA->targetIsBonus = false;
}
