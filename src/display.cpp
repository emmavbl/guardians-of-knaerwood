#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE 

#include "display.h"
#include "constantes.h"
#include "load.h"

#include <SDL/SDL_ttf.h>
#include "GL/glew.h"
#include <iostream>


void displayTexture(GLuint Texture[], double xSrc, double ySrc, double xDst, double yDst, int w_dst, int h_dst) {

	double image_w = Texture[1];
	double image_h = Texture[2];

    double xGL = 0.0;
    double yGL = 0.0;

    if (aspectRatio >= 1) {         
        double left = -GL_VIEW_SIZE / 2. * aspectRatio; 
        double right = GL_VIEW_SIZE / 2. * aspectRatio;
        double bottom = -GL_VIEW_SIZE / 2.;
        double top = GL_VIEW_SIZE / 2.;
        xGL = left + (right - left) * xDst / (float)WINDOW_WIDTH;  
        yGL = -(bottom + (top - bottom) * yDst / (float)WINDOW_HEIGHT);
    }
    else if (aspectRatio != 0) {
        double left = -GL_VIEW_SIZE / 2.;
        double right = GL_VIEW_SIZE / 2.;
        double bottom = -GL_VIEW_SIZE / 2. / aspectRatio;
        double top = GL_VIEW_SIZE / 2. / aspectRatio;
        xGL = left + (right - left) * xDst / (float)WINDOW_WIDTH;
        yGL = -(bottom + (top - bottom) * yDst / (float)WINDOW_HEIGHT);
        yGL /= aspectRatio;
    }
    

		glBindTexture(GL_TEXTURE_2D, Texture[0]);
			glBegin(GL_QUADS);
				glTexCoord2f((xSrc / image_w), (ySrc / image_h) + (h_dst / image_h)); glVertex2f(xGL, yGL - h_dst);                         // BAS GAUCHE
				glTexCoord2f(xSrc / image_w, ySrc / image_h); glVertex2f(xGL, yGL);                                               // HAUT GAUCHE
				glTexCoord2f((xSrc / image_w) + (w_dst / image_w), ySrc / image_h); glVertex2f(xGL + w_dst, yGL);                       // HAUT DROIT
				glTexCoord2f((xSrc / image_w) + (w_dst / image_w), (ySrc / image_h) + (h_dst / image_h)); glVertex2f(xGL + w_dst, yGL - h_dst);   // BAS DROIT
			glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
  

}


void displayChangeDay(int day, GLuint Texture[], int timeCountDay) {
    //char dayNumber[5];
    //itoa(day, dayNumber, 10);

    int dayNumber = day;

    std::string text = "Jour " + std::to_string(dayNumber);

    //text += dayNumber;

    if (timeCountDay == 1)
        loadFont(text.c_str(), 120, "R", Texture);

    displayTexture(Texture, 0, 0, WINDOW_WIDTH / 2 - (6 * TAILLE_BLOC), WINDOW_HEIGHT / 2 - (2 * TAILLE_BLOC), Texture[1], Texture[2]);
}

/*
Affiche les panneaux de descriptions de l'unit� selectionnee
*/
void displayDescUnit(Unit unit, Hud* hud) {

    int origin = 16;

    if (hud->side == "right") {
        origin = WINDOW_WIDTH - 16 - hud->pannelDescBlue[1];
    }

    if (unit.playerID == "B")
        displayTexture(hud->pannelDescBlue, 0, 0, origin, WINDOW_HEIGHT - (5 * TAILLE_BLOC) - 16, hud->pannelDescBlue[1], hud->pannelDescBlue[2]);
    else
        displayTexture(hud->pannelDescRed, 0, 0, origin, WINDOW_HEIGHT - (5 * TAILLE_BLOC) - 16, hud->pannelDescBlue[1], hud->pannelDescBlue[2]);


    if (hud->changeDescription) {
        std::string type = unit.type;
        std::string lvl = "lvl " + std::to_string(unit.lvl);
        std::string pv = std::to_string(unit.pv) + " pv";
        std::string power = std::to_string(unit.power) + " pa";

        loadFont(type.c_str(), 32, "W", hud->type);
        loadFont(lvl.c_str(), 24, "W", hud->lvl);
        loadFont(pv.c_str(), 24, "W", hud->pv);
        loadFont(power.c_str(), 24, "W", hud->power);
    }

    displayTexture(hud->type, 0, 0, origin + 19, 550, hud->type[1], hud->type[2]);
    displayTexture(hud->lvl, 0, 0, origin + 64, WINDOW_HEIGHT - 16 - 3 * TAILLE_BLOC - 8, hud->lvl[1], hud->lvl[2]);
    displayTexture(hud->pv, 0, 0, origin + 64, WINDOW_HEIGHT - 16 - 2 * TAILLE_BLOC - 8, hud->pv[1], hud->pv[2]);
    displayTexture(hud->power, 0, 0, origin + 64, WINDOW_HEIGHT - 16 - TAILLE_BLOC - 8, hud->power[1], hud->power[2]); // Margin � enlever
}

void displayMapAndUnits(Game* game, std::vector<Unit*> units, int seconds, GLuint Carte[], GLuint Grille[]) {
    displayTexture(Carte, 0, 0, 0, 0, Carte[1], Carte[2]);
    //displayTexture(Grille, 0, 0, 0, 0, Grille[1], Grille[2]);

    for (int i = 0; i < units.size(); i++) {
        if (units[i]->pv > 0)
            displayUnit(*units[i], seconds);
    }
}


void displayHUD(Game* game, Hud* hud, int& timeCountDay, int seconds, std::vector<Unit*> units) {

    if (game->caseX <= 20 * TAILLE_BLOC && hud->side == "left") {
        hud->side = "right";
    }
    else if (game->caseX > 20 * TAILLE_BLOC && hud->side == "right") {
        hud->side = "left";
    }

    // Texture du Cursor
    displayTexture(hud->cursorTexture, TAILLE_BLOC * hud->cursorID, 0, game->cursorX, game->cursorY, hud->cursorTexture[1] / 4, hud->cursorTexture[2]);

    // Cursor Case Selector
    int spriteSelector = seconds % 4;
    displayTexture(hud->selector, spriteSelector * 36, 0, (game->caseX) - 2, (game->caseY) - 2, hud->selector[1] / 4, hud->selector[2]);

    // Panneaux de description de l'unite selectionnee
    if (game->idSelect != -1 && !game->modeMove && !game->modeFight) {
        // Si on change d'unit� selectionnee ou pas
        if (game->idDescription == game->idSelect)
            hud->changeDescription = false;
        else {
            game->idDescription = game->idSelect;
            hud->changeDescription = true;
        }

        displayDescUnit(*units[game->idSelect], hud);
    }

    // Pannel Player (HAUT DROIT)
    if (game->idTurn == "B") {
        displayTexture(hud->pannelPlayerBlue, 0, 0, WINDOW_WIDTH - 8 * TAILLE_BLOC, 0, hud->pannelPlayerBlue[1], hud->pannelPlayerBlue[2]);
    }
    else {
        displayTexture(hud->pannelPlayerRed, 0, 0, WINDOW_WIDTH - 8 * TAILLE_BLOC, 0, hud->pannelPlayerRed[1], hud->pannelPlayerRed[2]);
    }
    displayTexture(hud->pseudo, 0, 0, WINDOW_WIDTH - 4.5 * TAILLE_BLOC, 20, hud->pseudo[1], hud->pseudo[2]);
    displayTexture(hud->money, 0, 0, WINDOW_WIDTH - 3.5 * TAILLE_BLOC, 58, hud->money[1], hud->money[2]);

    //Affichage de l'�cran histoire
    if (game->dayCount == 0 && !game->modeTuto)
        displayTexture(hud->story, 0, 0, 0, 0, hud->story[1], hud->story[2]);


    // Changement de jour
    if (game->changeDay) {
        timeCountDay++;
        displayChangeDay(game->dayCount, game->textureChangeDay, timeCountDay);
        if (timeCountDay > 180) {
            game->changeDay = false;
            game->dayCount++;
            timeCountDay = 0;
        }
    }
}


void displayMenuAuberge(Menu* menu, Game* game, GLuint prixLevel[][3], GLuint level[][3], GLuint playerMoney[])
{
    if (menu->reloadMenu) { // Reload = lorsqu'on levelUp / ach�te
        // Argent du joueur 
        std::string money;
        // Boucle sur les types d'unit�s
        for (int i = 0; i < 6; i++) {
            std::string price;
            std::string lvl = "lvl ";
            if (game->idTurn == "B") {
                lvl += std::to_string(game->playerBlue->arrayLevels[i] + 1);
                price = std::to_string(game->priceUnit[i] * (game->playerBlue->arrayLevels[i] + 1) / 2);
                money = std::to_string(game->playerBlue->money);
            }
            else {
                lvl += std::to_string(game->playerRed->arrayLevels[i] + 1);
                price = std::to_string(game->priceUnit[i] * (game->playerRed->arrayLevels[i] + 1) / 2);
                money = std::to_string(game->playerRed->money);
            }
            loadFont(price.c_str(), 21, "W", prixLevel[i]);
            loadFont(lvl.c_str(), 21, "W", level[i]);
        }
        loadFont(money.c_str(), 40, "W", playerMoney);
        menu->reloadMenu = false;
    }

    displayTexture(playerMoney, 0, 0, 35 * TAILLE_BLOC, 3 * TAILLE_BLOC, playerMoney[1], playerMoney[2]);

    // Prix des unit�s
    displayTexture(prixLevel[0], 0, 0, 18 * TAILLE_BLOC, 10.25 * TAILLE_BLOC, prixLevel[0][1], prixLevel[0][2]);
    displayTexture(prixLevel[1], 0, 0, 27 * TAILLE_BLOC, 10.25 * TAILLE_BLOC, prixLevel[1][1], prixLevel[1][2]);
    displayTexture(prixLevel[2], 0, 0, 18 * TAILLE_BLOC, 12.25 * TAILLE_BLOC, prixLevel[2][1], prixLevel[2][2]);
    displayTexture(prixLevel[3], 0, 0, 27 * TAILLE_BLOC, 12.25 * TAILLE_BLOC, prixLevel[3][1], prixLevel[3][2]);
    displayTexture(prixLevel[4], 0, 0, 18 * TAILLE_BLOC, 14.25 * TAILLE_BLOC, prixLevel[4][1], prixLevel[4][2]);
    displayTexture(prixLevel[5], 0, 0, 27 * TAILLE_BLOC, 14.25 * TAILLE_BLOC, prixLevel[5][1], prixLevel[5][2]);

    // Levels des unit�s
    displayTexture(level[0], 0, 0, 14 * TAILLE_BLOC, 10.25 * TAILLE_BLOC, level[0][1], level[0][2]);
    displayTexture(level[1], 0, 0, 23 * TAILLE_BLOC, 10.25 * TAILLE_BLOC, level[1][1], level[1][2]);
    displayTexture(level[2], 0, 0, 14 * TAILLE_BLOC, 12.25 * TAILLE_BLOC, level[2][1], level[2][2]);
    displayTexture(level[3], 0, 0, 23 * TAILLE_BLOC, 12.25 * TAILLE_BLOC, level[3][1], level[3][2]);
    displayTexture(level[4], 0, 0, 14 * TAILLE_BLOC, 14.25 * TAILLE_BLOC, level[4][1], level[4][2]);
    displayTexture(level[5], 0, 0, 23 * TAILLE_BLOC, 14.25 * TAILLE_BLOC, level[5][1], level[5][2]);

}


void displayEndGame(Game* game, std::string textEndgame, const char* colorEndgame, GLuint endGameText[], bool& firstFrameEndgame, GLuint pressEnter[])
{
    if (game->playerBlue->nbUnits == 0) {
        textEndgame = game->playerRed->name;
        textEndgame += " est victorieux";
        colorEndgame = "R";
    }
    else {
        textEndgame = game->playerBlue->name;
        textEndgame += " est victorieux";
        colorEndgame = "B";
    }

    // Chargement de la FOnt seuleument � la premi�re Frame
    if (firstFrameEndgame) {
        loadFont(textEndgame.c_str(), 100, colorEndgame, endGameText);
        firstFrameEndgame = false;
    }

    displayTexture(pressEnter, 0, 0, 12 * TAILLE_BLOC, WINDOW_HEIGHT / 2 + (2 * TAILLE_BLOC), pressEnter[1], pressEnter[2]);
    displayTexture(endGameText, 0, 0, 4 * TAILLE_BLOC, WINDOW_HEIGHT / 2 - (2 * TAILLE_BLOC), endGameText[1], endGameText[2]);
}


void displayMenuCaserne(Menu* menu, Game* game, GLuint playerMoney[]) {

    std::string money;
    if (menu->reloadMenu) {
        if (game->idTurn == "B")
            money = std::to_string(game->playerBlue->money);

        else
            money = std::to_string(game->playerRed->money);

        loadFont(money.c_str(), 40, "W", playerMoney);
        menu->reloadMenu = false;
    }

    displayTexture(playerMoney, 0, 0, 35 * TAILLE_BLOC, 3 * TAILLE_BLOC, playerMoney[1], playerMoney[2]);
}



