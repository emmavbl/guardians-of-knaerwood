#include "heap.h"
#include "mouvement.h"

#include <iostream>
#include <cstdlib>
#include <vector>
#include <iterator>


using namespace std;

int BHeap::Size() {
    return heap.size();
}
void BHeap::Insert(Cell node) {
    heap.push_back(node);
    heapifyup(heap.size() - 1);
}
void BHeap::DeleteMin() {
    if (heap.size() == 0) {
        return;
    }
    heap[0] = heap.at(heap.size() - 1);
    heap.pop_back();
    heapifydown(0);
}
Cell* BHeap::ExtractMin() {
    if (heap.size() == 0) {
        cout << "Erreur d'extraction : la liste est nulle" << endl;
        return NULL;
    }
    else
        return &heap.front();
}
void BHeap::showHeap() {
    cout << "Heap --> " << endl;
    for (int i = 0; i < heap.size(); i++)
        cout << "(" << heap[i].x / TAILLE_BLOC << "," << heap[i].y / TAILLE_BLOC << ") F " << heap[i].F << endl;
}
int BHeap::leftChild(int nodeIndex) {
    int i = 2 * nodeIndex + 1;
    if (i < heap.size())
        return i;
    else
        return -1;
}
int BHeap::rightChild(int nodeIndex) {
    int i = 2 * nodeIndex + 2;
    if (i < heap.size())
        return i;
    else
        return -1;
}
int BHeap::par(int child) {
    int p = (child - 1) / 2;
    if (child == 0)
        return -1;
    else
        return p;
}

void BHeap::heapifyup(int i) {

    if (i && heap[par(i)].F > heap[i].F)
    {
        swap(heap[i], heap[par(i)]);

        heapifyup(par(i));
    }

}


void BHeap::heapifydown(int i) {
    int left = leftChild(i);
    int right = rightChild(i);

    int smallest = i;


    if (left < heap.size() && heap[left].F < heap[i].F)
        smallest = left;

    if (right < heap.size() && heap[right].F < heap[smallest].F)
        smallest = right;

    if (smallest != i) {
        swap(heap[i], heap[smallest]);
        heapifydown(smallest);
    }
}
bool BHeap::heapSearch(Cell c, int currentIndex) {
    if (heap.size() == 0) {
        return false;
    }

    if (c.F == heap[0].F) {
        return true;
    }

    for (int i = 0; i < heap.size(); i++) {
        if (c.x == heap[i].x && c.y == heap[i].y) {
            return true;
        }
    }

    return false;
}


int BHeap::heapSearchIndex(Cell c, int currentIndex) {
    if (heap.size() == 0) {
        return -1;
    }

    for (int i = 0; i < heap.size(); i++) {
        if (c.x == heap[i].x && c.y == heap[i].y) {
            return i;
        }
    }

    return -1;
}


Cell* BHeap::parentNode(int index) {
    return heap[index].parent;
}

Cell* BHeap::value(int index) {
    return &heap[index];
}
