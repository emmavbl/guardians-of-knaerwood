#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE 

#include "units.h"
#include "constantes.h"
#include "load.h"
#include "display.h"
#include "game.h"
#include "IA.h"


#include "GL/glew.h"
#include <iostream>
#include <string>


void displayUnit(Unit unit, int time) {
	displayTexture(unit.texturePv, 0, 0, unit.x, unit.y - 5., unit.texturePv[1], unit.texturePv[2]);

	// Affichage des unit�s
	int sprite = time % unit.frames;
	displayTexture(unit.texture, sprite * TAILLE_BLOC, unit.line * TAILLE_BLOC, unit.x, unit.y, TAILLE_BLOC, TAILLE_BLOC);
}

void moveUnit(Unit* unit, int x, int y) {
	
	if (unit->x < x) {
		unit->x += 2;
	}
	else if (unit->x > x) {
		unit->x -= 2;
	}
	
	if (unit->y < y) {
		unit->y += 2;
	}
	else if (unit->y > y) {
		unit->y -= 2;
	}
}



Unit* createUnit(GLuint texture[], int frames, int line, int x, int y, int id, const char* playerID, GLuint texturePV[], const char* type, const char * typeRange, int lvl) {
	Unit* unit = (Unit*)malloc(sizeof(Unit));
	unit->texture[0] = texture[0];
	unit->texture[1] = texture[1];
	unit->texture[2] = texture[2];

	unit->texturePv[0] = texturePV[0];
	unit->texturePv[1] = texturePV[1];
	unit->texturePv[2] = texturePV[2];

	unit->x = x;
	unit->y = y;
	unit->id = id;
	unit->playerID = playerID;
	unit->line = line;
	unit->frames = frames;

	unit->type = type;
	unit->lvl = lvl;
	unit->typeRange = typeRange;

	unit->target = NULL;
	unit->priorite = 4;
	unit->targetID = -1;

	if (type == "Sorciere") {
		unit->pv = 80 + (lvl - 1) * 5;
		unit->power = 40 + (lvl - 1) * 2;
		unit->rangeMax = 7;
	}
	if (type == "Fee") {
		unit->pv = 150 + (lvl - 1) * 5;
		unit->power = 10 + (lvl - 1) * 2;
		unit->rangeMax = 7;

	}
	if (type == "Farfadet") {
		unit->pv = 90 + (lvl - 1) * 5;
		unit->power = 30 + (lvl - 1) * 2;
		unit->rangeMax = 3;

	}
	if (type == "Demon") {
		unit->pv = 100 + (lvl - 1) * 5;
		unit->power = 20 + (lvl - 1) * 2;
		unit->rangeMax = 3;


	}
	if (type == "Guerrier") {
		unit->pv = 100 + (lvl - 1) * 5;
		unit->power = 25 + (lvl - 1) * 2;
		unit->rangeMax = 5;

	}
	if (type == "Orc") {
		unit->pv = 120 + (lvl - 1) * 5;
		unit->power = 40 + (lvl - 1) * 2;
		unit->rangeMax = 5;

	}


	return unit;
}

//Attaque d'unit�
void fightUnits(Unit* attaquant, Unit* cible, Game* game, std::vector<Unit*>& units, int idCible, std::vector<Cell>& arrayObs) {

	cible->pv -= attaquant->power * ((float)attaquant->pv / 100.);

	// Si la cible est morte
	if (cible->pv <= 0) {
		attaquant->target = NULL;
		units.erase(units.begin() + idCible);
		updateObstacles(units, arrayObs, game);
		
		cible->pv = 0;

		if (game->idTurn == "B")
			game->playerRed->nbUnits--;
		else
			game->playerBlue->nbUnits--;

		return;
	}

	attaquant->pv -= cible->power * ((float)cible->pv / 200.);

	// Si l'attaquant est morte
	if (attaquant->pv <= 0) {
		units.erase(units.begin() + game->idSelect);
		updateObstacles(units, arrayObs, game);

		attaquant->pv = 0;
		if (game->idTurn == "B")
			game->playerBlue->nbUnits--;
		else
			game->playerRed->nbUnits--;
	}

	// Upload des points de vies
	if (attaquant->pv > 0) {
		int pvTextAtt = attaquant->pv;
		loadFont(std::to_string(pvTextAtt).c_str(), 16, attaquant->playerID, attaquant->texturePv);
	}
	

	int pvTextCbl = cible->pv;
	loadFont(std::to_string(pvTextCbl).c_str(), 16, cible->playerID, cible->texturePv);
	
}

void initPv(Unit* unit) {

	int pvText = unit->pv;
	loadFont(std::to_string(pvText).c_str(), 16, unit->playerID, unit->texturePv);
}

void levelUp(std::vector<Unit*> units, const char* idTurn, int idLevelUp){
	const char* type;

	switch(idLevelUp) {
	case 0:
		type = "Sorciere"; 
		break;

	case 1:
		type = "Fee" ; 
		break;

	case 2:
		type = "Farfadet"; 
		break;

	case 3:
		type = "Demon"; 
		break;

	case 4:
		type = "Guerrier";
		break;

	case 5:
		type = "Orc"; 
		break;

	default:
		type = "None";
		break;

	}

	for (int i = 0; i < units.size(); i++) {
		if (units[i]->type == type && units[i]->playerID == idTurn) {
			units[i]->lvl++;
			units[i]->pv += 5;
			initPv(units[i]);
			units[i]->power += 2;
		}
	}
	
}


void buyUnitInCaserne(Game * game, Menu * menu, IA * playerIA, std::vector<Cell>& arrayObs, std::vector<Unit*>& units, GLuint texturePV[],
	GLuint textureWitch[], GLuint textureFee[], GLuint textureFarfadet[], GLuint textureDemon[], GLuint textureGladiator[], GLuint textureOrc[]) {
	// MODE ACHAT
	if (game->modeAchat) {

		if (game->IA && game->idTurn == "R") {
			playerIA->nbUnits++;
				
		}

		//generate rand posx posy en fonction du joueur
		int randx = 0;
		int randy = 0;
		int level[6];


		Cell* birthCase = (Cell*)malloc(sizeof(Cell));

		if (game->idTurn == "B") {
			randx = 8;
			randy = 3;
			game->playerBlue->nbUnits++;
			for (int i = 0; i < 6; i++)
				level[i] = game->playerBlue->arrayLevels[i];


			initCell(randx * TAILLE_BLOC, randy * TAILLE_BLOC, birthCase, true);

			while (isInside(*birthCase, arrayObs)) {
				randx = rand() % 10;
				randy = rand() % 7;
				birthCase->x = randx * TAILLE_BLOC;
				birthCase->y = randy * TAILLE_BLOC;
			}
		}
		else {
			randx = 39;
			randy = 22;

			game->playerRed->nbUnits++;
			for (int i = 0; i < 6; i++)
				level[i] = game->playerRed->arrayLevels[i];


			initCell(randx * TAILLE_BLOC, randy * TAILLE_BLOC, birthCase, true);
			while (isInside(*birthCase, arrayObs)) {
				randx = 30 + rand() % 10;
				randy = 13 + rand() % 10;
				birthCase->x = randx * TAILLE_BLOC;
				birthCase->y = randy * TAILLE_BLOC;
			}

		}

		Unit* unit;
		//create unit
		switch (menu->idAchat) {
		case 0:
			unit = createUnit(textureWitch, 4, 5, randx * TAILLE_BLOC, randy * TAILLE_BLOC, units.size(), game->idTurn, texturePV, "Sorciere", "cross", level[0]);
			units.push_back(unit);
			initPv(units[units.size() - 1]);
			break;
		case 1:
			unit = createUnit(textureFee, 8, 1, randx * TAILLE_BLOC, randy * TAILLE_BLOC, units.size(), game->idTurn, texturePV, "Fee", "cross", level[1]);
			units.push_back(unit);
			initPv(units[units.size() - 1]);
			break;
		case 2:
			unit = createUnit(textureFarfadet, 7, 0, randx * TAILLE_BLOC, randy * TAILLE_BLOC, units.size(), game->idTurn, texturePV, "Farfadet", "damier", level[2]);
			units.push_back(unit);
			initPv(units[units.size() - 1]);
			break;
		case 3:
			unit = createUnit(textureDemon, 7, 0, randx * TAILLE_BLOC, randy * TAILLE_BLOC, units.size(), game->idTurn, texturePV, "Demon", "damier", level[3]);
			units.push_back(unit);
			initPv(units[units.size() - 1]);
			break;
		case 4:
			unit = createUnit(textureGladiator, 5, 0, randx * TAILLE_BLOC, randy * TAILLE_BLOC, units.size(), game->idTurn, texturePV, "Guerrier", "star", level[4]);
			units.push_back(unit);
			initPv(units[units.size() - 1]);
			break;
		case 5:
			unit = createUnit(textureOrc, 5, 0, randx * TAILLE_BLOC, randy * TAILLE_BLOC, units.size(), game->idTurn, texturePV, "Orc", "star", level[5]);
			units.push_back(unit);
			initPv(units[units.size() - 1]);
			break;
		}

		updateObstacles(units, arrayObs, game);

		game->modeAchat = false;
	}
}



