#include "load.h"

#include "SDL2/SDL.h"
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include "GL/glew.h"

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <string.h>


SDL_Surface* flipSurface(SDL_Surface* surface);


GLuint loadTexture(const char* filename, GLuint Texture[]) {

    /* Chargement de l'image */
    SDL_Surface* image = IMG_Load(filename);


    if (NULL == image) {
	printf("TTF_OpenFont: %s\n", TTF_GetError());
        return -1;
    }

    Texture[1] = image->w;
    Texture[2] = image->h;


    /* Initialisation de la texture */
    GLuint glID;
    glGenTextures(1, &glID);

    glBindTexture(GL_TEXTURE_2D, glID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


    GLenum format;
    switch (image->format->BytesPerPixel) {
    case 1:
        format = GL_RED;
        break;
    case 3:
        format = GL_RGB;
        break;
    case 4:
        format = GL_RGBA;
        break;
    default:
        fprintf(stderr, "Format des pixels de l'image %s non supporte.\n", filename);
        return -1;
    }
    

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);
    
    glBindTexture(GL_TEXTURE_2D, 0);

    /* Liberation de la m�moire occupee par img */
    SDL_FreeSurface(image);

    Texture[0] =  glID;

}


GLuint loadFont(const char* text, int size, const char* color, GLuint Texture[]) {
    
    TTF_Font* font = TTF_OpenFont("fonts/alagard.ttf", size);

    if (font == NULL) {
        fprintf(stderr, "Erreur Imporation Font : %s \n", TTF_GetError());
        return NULL;
    }

    SDL_Color c = { 0, 0, 0 };

    if (color == "W") {
        SDL_Color white = { 255, 255, 255 };
        c = white;
    }
    else if (color == "B") {
        SDL_Color blue = { 220, 15, 0 };
        c = blue;
    }
    else if (color == "N") {
        SDL_Color blue = { 0, 0, 0 };
        c = blue;
    }
    else if (color == "R") {
        SDL_Color red = { 15, 11, 158 };
        c = red;
    }

    SDL_Surface* surfaceFont = TTF_RenderUTF8_Blended(font, text, c);

    //TTF_CloseFont(font);
    
    if (NULL == surfaceFont)
    {
        fprintf(stderr, "Erreur Surface Font : %s", SDL_GetError());
        return NULL;
    }

    
    Texture[1] = surfaceFont->w;
    Texture[2] = surfaceFont->h;
    

    /* Initialisation de la texture */
    GLuint glID;
    glGenTextures(1, &glID);

    glBindTexture(GL_TEXTURE_2D, glID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


    GLenum format;
    switch (surfaceFont->format->BytesPerPixel) {
    case 1:
        format = GL_RED;
        break;
    case 3:
        format = GL_RGB;
        break;
    case 4:
        format = GL_RGBA;
        break;
    default:
        fprintf(stderr, "Format des pixels de l'image font non supporte.\n");
        return -1;
    }


    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surfaceFont->w, surfaceFont->h, 0, format, GL_UNSIGNED_BYTE, surfaceFont->pixels);

    glBindTexture(GL_TEXTURE_2D, 0);

    /* Liberation de la m�moire occupee par img */
    SDL_FreeSurface(surfaceFont);


    Texture[0] = glID;
   
}

SDL_Surface* flipSurface(SDL_Surface* surface)
{
    int current_line, pitch;
    SDL_Surface* fliped_surface = SDL_CreateRGBSurface(SDL_SWSURFACE,
        surface->w, surface->h,
        surface->format->BitsPerPixel,
        surface->format->Rmask,
        surface->format->Gmask,
        surface->format->Bmask,
        surface->format->Amask);



    SDL_LockSurface(surface);
    SDL_LockSurface(fliped_surface);

    pitch = surface->pitch;
    for (current_line = 0; current_line < surface->h; current_line++)
    {
        memcpy(&((unsigned char*)fliped_surface->pixels)[current_line * pitch],
            &((unsigned char*)surface->pixels)[(surface->h - 1 -
                current_line) * pitch],
            pitch);
    }

    SDL_UnlockSurface(fliped_surface);
    SDL_UnlockSurface(surface);
    return fliped_surface;
}
