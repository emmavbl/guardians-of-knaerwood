# Guardians of Knaerwoods

Guardians of Knaerwood est un jeu de stratégie action devellopé en C++ à l'aide de la librairie SDL et OpenGL. L'histoire se déroule dans le mythique Royaume de Knaerwoods, royaume déchu d'Alexander Soulimac. 

Défendez l'héritage de Soulimac ou revendiquez l'Epée sacrée !

## Compilation

Pour compiler le jeu sur Linux vous devez avoir installer les librairies SDL 2.0 et OpenGL. Si vous ne l’avez pas déjà fait, entrez les commandes suivantes dans votre terminal : 

```
$ sudo install apt-get update
$ sudo apt install libsdl2-2.0-0 libsdl2-gfx-1.0-0 libsdl2-image-2.0-0 libsdl2-mixer-2.0-0 libsdl2-net-2.0-0 libsdl2-ttf-2.0-0
$ sudo apt-get install libsdl2-ttf-dev
$ sudo apt install libsdl2-image-dev
$ sudo apt-get install libglfw3-dev libgl1-mesa-dev libglu1-mesa-dev
$ sudo apt-get install freeglut3-dev
```

Ensuite placez votre terminal dans le dossier source et lancer les commandes suivantes pour compiler et lancer le jeu.  

```
$ make 
$ make clean
$ ./GoK
```

## Auteurs

* **Thomas Zorroché**
* **Emma Vodable**



